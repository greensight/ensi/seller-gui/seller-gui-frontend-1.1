# Шаблон чарта для сервиса на базе laravel

Данный чарт позволяет развернуть web-приложение на базе laravel.
При активации всех опций чарт создаёт:

-   Job (helm hook) для применения миграций
-   Deployment, поды которого состоят из контейнеров php-fpm и nginx
-   Service
-   Deployment'ы для запуска фоновых процессов, например обработчиков очереди
-   CronJob'ы для запуска периодических задач

Все env переменные и точки монтирования задаются в values.yaml.

| Параметр                                      |                                              |
| --------------------------------------------- | -------------------------------------------- |
| app.image.repository                          | адрес образа приложения                      |
| app.image.tag                                 | тэг образа приложения                        |
| app.image.pullPolicy                          | политиика скачивания образа                  |
| app.resources                                 | ресурсы контейнера приложения                |
| app.env                                       | переменные окружения контейнера приложения   |
| app.extraVolumes                              | дополнительные volume контейнера приложения  |
| app.extraVolumeMounts                         | точки монтирования дополнительных volume     |
| app.background_processes                      | список фоновых процессов приложения          |
| app.background_processes.example.enabled      | флаг активности фонового процесса            |
| app.background_processes.example.replicas     | количество реплик фонового процесса          |
| app.background_processes.example.command      | команда фонового процесса                    |
| app.background_processes.example.resources    | ресурсы фонового процесса                    |
| app.cronjobs                                  | список периодических задач                   |
| app.cronjobs.example.enabled                  | флаг активности периодической задачи         |
| app.cronjobs.example.schedule                 | расписание периодической задачи              |
| app.cronjobs.example.command                  | команда периодической задачи                 |
| app.cronjobs.example.resources                | ресурсы периодической задачи                 |
| nginx.image.repository                        | адрес образа nginx                           |
| nginx.image.tag                               | тэг образа nginx                             |
| nginx.image.pullPolicy                        | политика скачивания образа nginx             |
| nginx.resources                               | ресурсы контейнера nginx                     |
| nginx.env                                     | переменные окружения контейнера nginx        |
| nginx.extraVolumes                            | дополнительные volume контейнера nginx       |
| nginx.extraVolumeMounts                       | точки монтирования дополнительных volume     |
| pre_install.enabled                           | флаг активности хука pre-install             |
| pre_install.command                           | команда хука pre-install                     |
| pre_install.image.repository                  | адрес образа хука pre-install                |
| pre_install.image.tag                         | тэг образа хука pre-install                  |
| pre_install.image.pullPolicy                  | политика скачивания образа хука pre-install  |
| pre_install.resources                         | ресурсы контейнера хука pre-install          |
| pre_install.env                               | переменные окружения хука pre-install        |
| pre_install.extraVolumes                      | дополнительные volume контейнера pre-install |
| pre_install.extraVolumeMounts                 | точки монтирования дополнительных volume     |
| serviceAccount.create                         | создавать ли serviceAccount                  |
| serviceAccount.annotations                    | аннотации serviceAccount                     |
| serviceAccount.name                           | название serviceAccount                      |
| podAnnotations                                |                                              |
| podSecurityContext                            |                                              |
| securityContext                               |                                              |
| service.type                                  | тип Service                                  |
| service.port                                  | порт Service                                 |
| autoscaling.enabled                           | включить автомасштабирование приложения      |
| autoscaling.minReplicas                       | минимальное количество реплик                |
| autoscaling.maxReplicas                       | максимальное количество реплик               |
| autoscaling.targetCPUUtilizationPercentage    | значение CPU при котором мастабировать       |
| autoscaling.targetMemoryUtilizationPercentage | значение Memory при котором масштабировать   |
| nodeSelector                                  | привязка к конкретной ноде                   |
| tolerations                                   |                                              |
| affinity                                      |                                              |
