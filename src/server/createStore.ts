import { Request } from 'express';
import { matchPath } from 'react-router-dom';
import { configureStore } from '@reduxjs/toolkit';
import parser from 'ua-parser-js';
import { toArray } from '@scripts/helpers';
import reducer from '@reducers/index';
import { setUa } from '@reducers/ua';
import { setProfile } from '@reducers/profile';
import routes from '../client/pages';

const createStore = async (req: Request) => {
    const store = configureStore({ reducer });
    const { token } = (req as any).universalCookies.cookies;

    /* Заполняем стор статичными данными. */
    store.dispatch(setUa(parser(req.headers['user-agent']) as any));

    if (token) {
        store.dispatch(setProfile(token));
    }
    /* Загружаем данные, общие для всех страниц. */
    // await Promise.all([store.dispatch(fetchSomething())]);

    /* Загружаем данные для запрашиваемого роута. */
    let promises: Promise<any>[] = [];
    routes.some(route => {
        const match = matchPath(req.path, route);
        if (match && route.loadData) promises = toArray(route.loadData({ store, req }));
        return match;
    });
    await Promise.all(promises);

    return store;
};

export default createStore;
