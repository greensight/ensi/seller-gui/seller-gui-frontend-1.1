import React, { useState, useMemo } from 'react';
import { useLocation } from 'react-router-dom';
import { Button, scale, useTheme, Layout } from '@greensight/gds';
import { FormikValues } from 'formik';

import Table from '@components/Table';
import Block from '@components/Block';
import PageWrapper from '@components/PageWrapper';

import MultiSelect from '@standart/MultiSelect';
import Form from '@standart/Form';
import DatepickerStyles from '@standart/Datepicker/presets';
import DatepickerRange from '@standart/DatepickerRange';
import Pagination from '@standart/Pagination';

import typography from '@scripts/typography';
import { makeCargos, cargoStatuses, warehouses, deliveryServices } from '@scripts/mock';
import { prepareForSelect } from '@scripts/helpers';
import { useURLHelper } from '@scripts/useURLHelper';

const COLUMNS = [
    {
        Header: 'ID',
        accessor: 'id',
        getProps: () => ({ type: 'linkedID' }),
    },
    {
        Header: 'Дата создания',
        accessor: 'created',
        getProps: () => ({ type: 'date' }),
    },
    {
        Header: 'Служба доставки',
        accessor: 'delivery',
    },
    {
        Header: 'Склад',
        accessor: 'warehouse',
    },
    {
        Header: 'Статус',
        accessor: 'status',
        getProps: () => ({ type: 'status' }),
    },
    {
        Header: 'Кол-во коробок',
        accessor: 'boxes',
    },
    {
        Header: 'Комментарий',
        accessor: 'comment',
    },
];

const attrTypes = [
    { label: 'Не выбрано', value: '' },
    { label: 'Да', value: '1' },
    { label: 'Нет', value: '0' },
];

const DELIVERIES = prepareForSelect(deliveryServices);
const WAREHOUSES = prepareForSelect(warehouses);
const statuses = prepareForSelect(cargoStatuses);

const Filters = ({
    className,
    initialValues,
    emptyInitialValues,
    onSubmit,
    onReset,
}: {
    className?: string;
    onSubmit: (vals: FormikValues) => void;
    onReset: (vals: FormikValues) => void;
    emptyInitialValues: FormikValues;
    initialValues: FormikValues;
}) => {
    const { colors } = useTheme();
    const [moreFilters, setMoreFilters] = useState(true);

    return (
        <Block className={className}>
            <DatepickerStyles />
            <Form initialValues={initialValues} onSubmit={onSubmit} onReset={onReset}>
                <Block.Body>
                    <Layout cols={7}>
                        <Layout.Item col={1}>
                            <Form.Field name="cargoID" label="ID" />
                        </Layout.Item>
                        <Layout.Item col={2}>
                            <Form.Field name="cargoStatus" label="Статус груза">
                                <MultiSelect options={statuses} />
                            </Form.Field>
                        </Layout.Item>
                        <Layout.Item col={2}>
                            <Form.Field name="deliveryService" label="Служба доставки">
                                <MultiSelect options={DELIVERIES} />
                            </Form.Field>
                        </Layout.Item>
                        <Layout.Item col={2}>
                            <Form.Field name="warehouse" label="Склад отгрузки">
                                <MultiSelect options={WAREHOUSES} />
                            </Form.Field>
                        </Layout.Item>
                        {moreFilters ? (
                            <>
                                <Layout.Item col={1}>
                                    <Form.Field name="departureNo" label="№ заказа в грузе" />
                                </Layout.Item>
                                <Layout.Item col={4}>
                                    <Form.Field name="datepickerRange" label="Дата создания">
                                        <DatepickerRange />
                                    </Form.Field>
                                </Layout.Item>
                                <Layout.Item col={2}>
                                    <Form.Field name="canceled" label="Отменен">
                                        <MultiSelect options={attrTypes} isMulti={false} />
                                    </Form.Field>
                                </Layout.Item>
                            </>
                        ) : null}
                    </Layout>
                </Block.Body>
                <Block.Footer>
                    <div css={typography('bodySm')}>
                        Найдено 135 позиций{' '}
                        <button
                            type="button"
                            css={{ color: colors?.primary, marginLeft: scale(2) }}
                            onClick={() => setMoreFilters(!moreFilters)}
                        >
                            {moreFilters ? 'Меньше' : 'Больше'} фильтров
                        </button>
                    </div>
                    <div>
                        <Form.Reset size="sm" theme="secondary" type="button" initialValues={emptyInitialValues}>
                            Сбросить
                        </Form.Reset>
                        <Button size="sm" theme="primary" css={{ marginLeft: scale(2) }} type="submit">
                            Применить
                        </Button>
                    </div>
                </Block.Footer>
            </Form>
        </Block>
    );
};

const Cargos = () => {
    const { pathname, search } = useLocation();
    const activePage = +(new URLSearchParams(search).get('page') || 1);
    const data = useMemo(() => makeCargos(9), []);

    const emptyInitialValues = {
        cargoID: '',
        seller: null,
        cargoStatus: null,
        deliveryService: null,
        warehouse: null,
        departureNo: '',
        canceled: '',
        createdAt: '',
        datepickerRange: [null],
    };

    const { initialValues, URLHelper } = useURLHelper(emptyInitialValues);

    return (
        <PageWrapper h1="Грузы">
            <Filters
                onSubmit={vals => {
                    URLHelper(vals);
                }}
                onReset={vals => console.log('hoh')}
                initialValues={initialValues}
                emptyInitialValues={emptyInitialValues}
                css={{ marginBottom: scale(3) }}
            />
            <Block>
                <Block.Body>
                    <Table
                        columns={COLUMNS}
                        data={data}
                        needCheckboxesCol={false}
                        onSettingsClick={() => {
                            console.log('hoh');
                        }}
                    />
                    <Pagination url={pathname} activePage={activePage} pages={7} />
                </Block.Body>
            </Block>
        </PageWrapper>
    );
};

export default Cargos;
