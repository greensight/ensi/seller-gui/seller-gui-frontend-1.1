import React, { useState, useMemo } from 'react';
import { Button, scale, Layout } from '@greensight/gds';
import typography from '@scripts/typography';
import PageWrapper from '@components/PageWrapper';
import Table from '@components/Table';
import Block from '@components/Block';
import Form from '@standart/Form';
import MultiSelect from '@standart/MultiSelect';
import { makeClaimsContent } from '@scripts/mock';
import Pagination from '@standart/Pagination';
import { useLocation } from 'react-router-dom';
import Popup from '@standart/Popup';
import useSelectedRowsData from '@scripts/useSelectedRowsData';
import Legend from '@standart/Legend';
import Datepicker from '@standart/Datepicker';
import DatepickerStyles from '@standart/Datepicker/presets';

const claimTypes = [
    { label: 'Не важно', value: 'Не важно' },
    { label: 'Тестовое описание', value: 'Тестовое описание' },
    { label: 'Фото/видео съемка', value: 'Фото/видео съемка' },
    { label: 'Съёмка и текст', value: 'Съёмка и текст' },
];

const claimStatuses = [
    { label: 'Не важно', value: 'Не важно' },
    { label: 'Новая', value: 'Новая' },
    { label: 'В обработке', value: 'В обработке' },
    { label: 'В продакшен', value: 'В продакшен' },
    { label: 'Загрузка контента', value: 'Загрузка контента' },
    { label: 'Возврат на склад', value: 'Возврат на склад' },
    { label: 'Завершено', value: 'Завершено' },
    { label: 'Отклонена', value: 'Отклонена' },
];

const claimShootingTypes = [
    { label: 'Не важно', value: 'Не важно' },
    { label: 'Без вскрытия', value: 'Без вскрытия' },
    { label: 'Со вскрытием', value: 'Со вскрытием' },
];

const COLUMNS = [
    {
        Header: 'ID',
        accessor: 'id',
        getProps: () => ({ type: 'linkedID' }),
    },
    {
        Header: 'Тип',
        accessor: 'type',
    },
    {
        Header: 'Статус',
        accessor: 'status',
        getProps: () => ({ type: 'status' }),
    },
    {
        Header: 'Кол-во товаров',
        accessor: 'quantity',
    },
    {
        Header: 'Тип съёмки',
        accessor: 'shootingType',
    },
    {
        Header: 'Автор',
        accessor: 'claimAuthor',
        getProps: () => ({ type: 'double' }),
    },
    {
        Header: 'Создана',
        accessor: 'created',
        getProps: () => ({ type: 'date' }),
    },
    {
        Header: 'Документы',
        accessor: 'document',
        getProps: () => ({ type: 'documentDownload' }),
    },
];

const ClaimContent = () => {
    const { pathname, search } = useLocation();
    const activePage = +(new URLSearchParams(search).get('page') || 1);
    const [moreFilters, setMoreFilters] = useState(true);
    const [isAddClaimOpen, setIsAddClaimOpen] = useState(false);
    const data = useMemo(() => makeClaimsContent(9), []);
    const [ids, setIds, popupTableData] = useSelectedRowsData<typeof data[0]>(data);
    const [startCreationDate, setStartCreationDate] = useState<Date | null>(null);
    const [endCreationDate, setEndCreationDate] = useState<Date | null>(null);
    const [startDiscountDate, setStartDiscountDate] = useState<Date | null>(null);
    const [endDiscountDate, setEndDiscountDate] = useState<Date | null>(null);

    return (
        <PageWrapper h1="Заявки на производство контента">
            <DatepickerStyles />
            <main>
                <Block css={{ marginBottom: scale(3) }}>
                    <Form
                        initialValues={{
                            claimID: '',
                            claimTypes: [],
                            claimStatus: [],
                            claimAuthor: '',
                            claimShootingType: [],
                            startCreationDate: null,
                            endCreationDate: null,
                        }}
                        onSubmit={values => {
                            console.log(values);
                        }}
                    >
                        <Block.Body>
                            <Layout cols={9}>
                                <Layout.Item col={3}>
                                    <Form.Field name="claimID" label="ID заявки" />
                                </Layout.Item>
                                <Layout.Item col={3}>
                                    <Form.Field name="claimTypes" label="Тип заявки">
                                        <MultiSelect options={claimTypes} isMulti={false} />
                                    </Form.Field>
                                </Layout.Item>
                                <Layout.Item col={3}>
                                    <Form.Field name="claimStatus" label="Статус заявки">
                                        <MultiSelect options={claimStatuses} isMulti={false} />
                                    </Form.Field>
                                </Layout.Item>
                                {moreFilters ? (
                                    <>
                                        <Layout.Item col={3}>
                                            <Form.Field name="claimAuthor" label="Автор" />
                                        </Layout.Item>
                                        <Layout.Item col={2}>
                                            <Form.Field name="claimShootingType" label="Тип съемки">
                                                <MultiSelect options={claimShootingTypes} isMulti={false} />
                                            </Form.Field>
                                        </Layout.Item>
                                        <Layout.Item col={2}>
                                            <Form.FastField name="startCreationDate">
                                                <Legend label="Дата создания от" />
                                                <Datepicker
                                                    selectsStart
                                                    selected={startCreationDate}
                                                    startDate={startCreationDate}
                                                    endDate={endCreationDate}
                                                    maxDate={endCreationDate}
                                                    onChange={setStartCreationDate}
                                                />
                                            </Form.FastField>
                                        </Layout.Item>
                                        <Layout.Item col={2}>
                                            <Form.FastField name="endCreationDate">
                                                <Legend label="Дата создания до" />
                                                <Datepicker
                                                    selectsEnd
                                                    selected={endCreationDate}
                                                    startDate={startCreationDate}
                                                    endDate={endCreationDate}
                                                    minDate={startCreationDate}
                                                    onChange={setEndCreationDate}
                                                />
                                            </Form.FastField>
                                        </Layout.Item>
                                    </>
                                ) : null}
                            </Layout>
                        </Block.Body>
                        <Block.Footer>
                            <div css={typography('bodySm')}>
                                <Button theme="outline" size="sm" onClick={() => setMoreFilters(!moreFilters)}>
                                    {moreFilters ? 'Меньше' : 'Больше'} фильтров
                                </Button>
                            </div>
                            <div>
                                <Form.Reset
                                    size="sm"
                                    theme="secondary"
                                    type="button"
                                    onClick={() => {
                                        setStartCreationDate(null);
                                        setEndCreationDate(null);
                                        setStartDiscountDate(null);
                                        setEndDiscountDate(null);
                                    }}
                                >
                                    Сбросить
                                </Form.Reset>
                                <Button size="sm" theme="primary" css={{ marginLeft: scale(2) }} type="submit">
                                    Применить
                                </Button>
                            </div>
                        </Block.Footer>
                    </Form>
                </Block>

                <Block>
                    <Block.Header>
                        <div>
                            <Button
                                theme="primary"
                                size="sm"
                                css={{ marginRight: scale(2) }}
                                onClick={() => setIsAddClaimOpen(true)}
                            >
                                Создать заявку
                            </Button>
                        </div>
                    </Block.Header>
                    <Block.Body>
                        <Table
                            columns={COLUMNS}
                            data={data}
                            editRow={row => console.log('rowdata', row)}
                            onRowSelect={setIds}
                        />
                        <Pagination url={pathname} activePage={activePage} pages={7} css={{ marginTop: scale(2) }} />
                    </Block.Body>
                </Block>
            </main>
            <Popup
                isOpen={isAddClaimOpen}
                onRequestClose={() => {
                    setIsAddClaimOpen(false);
                }}
                title="Создание заявки на производство контента"
                popupCss={{ minWidth: scale(60) }}
            >
                <Form
                    onSubmit={values => {
                        setIsAddClaimOpen(false);
                        console.log(values);
                    }}
                    initialValues={{
                        offersIds: '',
                        claimTypes: [],
                        claimShootingTypes: [],
                    }}
                >
                    <Form.Field name="offersIds" label="ID офферов через запятую" css={{ marginBottom: scale(1) }} />
                    <Form.Field label="Тип производства контента" name="claimTypes" css={{ marginBottom: scale(2) }}>
                        <MultiSelect options={claimTypes} isMulti={false} />
                    </Form.Field>
                    <Form.Field label="Тип фотосъёмки" name="claimShootingTypes" css={{ marginBottom: scale(2) }}>
                        <MultiSelect options={claimShootingTypes} isMulti={false} />
                    </Form.Field>
                    <Button type="submit" size="sm" theme="primary">
                        Сохранить
                    </Button>
                </Form>
            </Popup>
        </PageWrapper>
    );
};

export default ClaimContent;
