import React, { useState, useMemo } from 'react';
import { useLocation } from 'react-router-dom';
import { Button, scale, useTheme, Layout } from '@greensight/gds';
import typography from '@scripts/typography';
import Table from '@components/Table';
import Block from '@components/Block';
import Form from '@standart/Form';
import MultiSelect from '@standart/MultiSelect';
import DatepickerStyles from '@standart/Datepicker/presets';
import Datepicker from '@standart/Datepicker';
import Legend from '@standart/Legend';
import Pagination from '@standart/Pagination';
import { makeImports, importTypes, importStatuses } from '@scripts/mock';

const STATUSES = importStatuses.map(i => ({ label: i, value: i }));
const TYPES = importTypes.map(i => ({ label: i, value: i }));

const ImportsFilter = () => {
    const { colors } = useTheme();
    const [moreFilters, setMoreFilters] = useState(true);

    return (
        <Block css={{ marginBottom: scale(2) }}>
            <Form
                initialValues={{
                    created: '',
                    statuses: [],
                    types: [],
                    start: '',
                    ending: '',
                }}
                onSubmit={values => {
                    console.log(values);
                }}
            >
                <Block.Body>
                    <Layout cols={3}>
                        <Layout.Item col={1}>
                            <Form.Field name="created">
                                <Legend label="Дата создания импорта" />
                                <Datepicker />
                            </Form.Field>
                        </Layout.Item>

                        <Layout.Item col={1}>
                            <Form.Field name="statuses" label="Статус импорта">
                                <MultiSelect options={STATUSES} />
                            </Form.Field>
                        </Layout.Item>

                        <Layout.Item col={1}>
                            <Form.Field name="types" label="Тип импорта">
                                <MultiSelect options={TYPES} />
                            </Form.Field>
                        </Layout.Item>

                        {moreFilters && (
                            <Layout.Item col={3}>
                                <Layout cols={3}>
                                    <Layout.Item col={1}>
                                        <Form.Field name="start">
                                            <Legend label="Дата начала импорта" />
                                            <Datepicker />
                                        </Form.Field>
                                    </Layout.Item>
                                    <Layout.Item col={1}>
                                        <Form.Field name="ending">
                                            <Legend label="Дата окончания импорта" />
                                            <Datepicker />
                                        </Form.Field>
                                    </Layout.Item>
                                </Layout>
                            </Layout.Item>
                        )}
                    </Layout>
                </Block.Body>

                <Block.Footer>
                    <div css={typography('bodySm')}>
                        Всего импортов: 5.{' '}
                        <button
                            type="button"
                            css={{ color: colors?.primary, marginLeft: scale(2) }}
                            onClick={() => setMoreFilters(!moreFilters)}
                        >
                            {moreFilters ? 'Меньше' : 'Больше'} фильтров
                        </button>{' '}
                    </div>

                    <div>
                        <Form.Reset
                            size="sm"
                            theme="secondary"
                            type="button"
                            onClick={() => {
                                console.log('reset');
                            }}
                        >
                            Сбросить
                        </Form.Reset>

                        <Button size="sm" theme="primary" css={{ marginLeft: scale(2) }} type="submit">
                            Применить
                        </Button>
                    </div>
                </Block.Footer>
            </Form>
        </Block>
    );
};

const COLUMNS = [
    {
        Header: 'ID',
        accessor: 'id',
        getProps: () => ({ type: 'linkedID' }),
    },
    {
        Header: 'Тип импорта',
        accessor: 'types',
    },
    {
        Header: 'Дата создания импорта',
        accessor: 'created',
        getProps: () => ({ type: 'date' }),
    },
    {
        Header: 'Статус импорта',
        accessor: 'statuses',
        getProps: () => ({ type: 'status' }),
    },
    {
        Header: 'Дата начала импорта',
        accessor: 'start',
        getProps: () => ({ type: 'date' }),
    },
    {
        Header: 'Дата окончания импорта',
        accessor: 'ending',
        getProps: () => ({ type: 'date' }),
    },
];

const ImportsTable = () => {
    const { pathname, search } = useLocation();
    const activePage = +(new URLSearchParams(search).get('page') || 1);
    const data = useMemo(() => makeImports(10), []);

    return (
        <Block>
            <Block.Body>
                <Table columns={COLUMNS} data={data} />
                <Pagination url={pathname} activePage={activePage} pages={7} />
            </Block.Body>
        </Block>
    );
};

const Imports = () => {
    return (
        <main css={{ flexGrow: 1, flexShrink: 1, padding: `${scale(2)}px ${scale(3)}px` }}>
            <DatepickerStyles />
            <h1 css={{ ...typography('h1'), marginBottom: scale(2) }}>Импорты</h1>
            <ImportsFilter />
            <ImportsTable />
        </main>
    );
};

export default Imports;
