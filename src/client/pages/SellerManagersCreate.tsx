import React from 'react';
import { Link, useParams } from 'react-router-dom';
import { Button, scale, Layout } from '@greensight/gds';
import * as Yup from 'yup';

import Block from '@components/Block';
import PageWrapper from '@components/PageWrapper';

import Form from '@standart/Form';
import Password from '@standart/Password';
import Switcher from '@standart/Switcher';

import { errorMessages } from '@scripts/errorMessages';

import PlusIcon from '@svg/plus.svg';
import ArrowLeft from '@svg/tokens/small/arrowLeft.svg';

const mockManager = {
    lastName: 'Иванов',
    firstName: 'Иван',
    middleName: 'Иванович',
    email: 'ivanov@ian.ru',
    password: '12344321',
    sms: true,
    active: true,
};

const SellerManagers = () => {
    const { id } = useParams<{ id?: string }>();

    const initialValues = {
        lastName: '',
        firstName: '',
        middleName: '',
        email: '',
        password: '',
        sms: false,
        active: false,
    };
    const shape = {
        lastName: Yup.string().required(errorMessages.required),
        firstName: Yup.string().required(errorMessages.required),
        middleName: Yup.string().required(errorMessages.required),
        email: Yup.string().email(errorMessages.email).required(errorMessages.required),
        password: Yup.string().required(errorMessages.required),
    };

    return (
        <PageWrapper h1={id ? `Редактировать менеджера ${mockManager.lastName}` : `Добавить менеджера`}>
            <Button
                Icon={ArrowLeft}
                to="/seller/managers"
                as={Link}
                size="sm"
                theme="secondary"
                css={{ marginBottom: scale(2) }}
            >
                Назад к менеджерам
            </Button>
            <Block>
                <Block.Body>
                    <Form
                        onSubmit={val => console.log(val)}
                        initialValues={id ? mockManager : initialValues}
                        validationSchema={Yup.object().shape(shape)}
                        css={{ maxWidth: scale(128) }}
                    >
                        <Layout cols={3}>
                            <Layout.Item col={1}>
                                <Form.FastField name="lastName" label="Фамилия" />
                            </Layout.Item>
                            <Layout.Item col={1}>
                                <Form.FastField name="firstName" label="Имя" />
                            </Layout.Item>
                            <Layout.Item col={1}>
                                <Form.FastField name="middleName" label="Отчество" />
                            </Layout.Item>
                            <Layout.Item col={3}>
                                <Form.FastField name="email" type="email" label="Email" />
                            </Layout.Item>
                            <Layout.Item col={3}>
                                <Form.FastField name="password" label="Пароль">
                                    <Password />
                                </Form.FastField>
                            </Layout.Item>
                            <Layout.Item col={3}>
                                <Form.FastField name="sms">
                                    <Switcher>Получать SMS</Switcher>
                                </Form.FastField>
                            </Layout.Item>
                            <Layout.Item col={3}>
                                <Form.FastField name="active">
                                    <Switcher>Активен</Switcher>
                                </Form.FastField>
                            </Layout.Item>
                            <Layout.Item col={3} justify="end">
                                <Button type="submit" size="sm" Icon={PlusIcon}>
                                    Добавить
                                </Button>
                            </Layout.Item>
                        </Layout>
                    </Form>
                </Block.Body>
            </Block>
        </PageWrapper>
    );
};

export default SellerManagers;
