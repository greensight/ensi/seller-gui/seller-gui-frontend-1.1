import React from 'react';
import { scale, useTheme, Layout } from '@greensight/gds';
import { CSSObject } from '@emotion/core';
import typography from '@scripts/typography';
import Block from '@components/Block';
import Badge from '@components/Badge';
import { format } from 'date-fns';
import { STATUSES } from '@scripts/enums';
import { Link } from 'react-router-dom';

const ImportDetail = () => {
    const { colors } = useTheme();

    const importMock = {
        id: 52,
        type: 'Импорт цен',
        date: new Date(),
        status: 'Выполнен',
        start: new Date(),
        ending: new Date(),
        params: '-',
        fileUrl: '#',
        result: 'Импорт успешно выполнен',
    };

    const itemCss: CSSObject = {
        padding: `${scale(2)}px ${scale(3)}px`,
        borderBottom: `1px solid ${colors?.grey200}`,
        ':last-of-type': { border: 'none', paddingBottom: 0 },
    };

    return (
        <main css={{ flexGrow: 1, flexShrink: 1, padding: `${scale(2)}px ${scale(3)}px` }}>
            <h1 css={{ ...typography('h1'), marginBottom: scale(2) }}>Импорты</h1>
            <Block>
                <Block.Body css={{ padding: 0 }}>
                    <Layout gap={0} cols={2}>
                        <Layout.Item css={itemCss} col={1}>
                            ID
                        </Layout.Item>
                        <Layout.Item css={itemCss} col={1}>
                            {importMock.id}
                        </Layout.Item>
                        <Layout.Item css={itemCss} col={1}>
                            Тип импорта
                        </Layout.Item>
                        <Layout.Item css={itemCss} col={1}>
                            {importMock.type}
                        </Layout.Item>
                        <Layout.Item css={itemCss} col={1}>
                            Дата создания импорта
                        </Layout.Item>
                        <Layout.Item css={itemCss} col={1}>
                            {format(importMock.date, 'dd.MM.yyyy HH:mm')}
                        </Layout.Item>
                        <Layout.Item css={itemCss} col={1}>
                            Статус импорта
                        </Layout.Item>
                        <Layout.Item css={itemCss} col={1}>
                            <Badge text={importMock.status} type={STATUSES.SUCCESS} />
                        </Layout.Item>
                        <Layout.Item css={itemCss} col={1}>
                            Дата начала импорта
                        </Layout.Item>
                        <Layout.Item css={itemCss} col={1}>
                            {format(importMock.start, 'dd.MM.yyyy HH:mm')}
                        </Layout.Item>
                        <Layout.Item css={itemCss} col={1}>
                            Дата окончания импорта
                        </Layout.Item>
                        <Layout.Item css={itemCss} col={1}>
                            {format(importMock.ending, 'dd.MM.yyyy HH:mm')}
                        </Layout.Item>
                        <Layout.Item css={itemCss} col={1}>
                            Параметры импорта
                        </Layout.Item>
                        <Layout.Item css={itemCss} col={1}>
                            {importMock.params}
                        </Layout.Item>
                        <Layout.Item css={itemCss} col={1}>
                            Файл
                        </Layout.Item>
                        <Layout.Item css={itemCss} col={1}>
                            <Link css={{ color: colors?.primary }} to={importMock.fileUrl}>
                                Файл
                            </Link>
                        </Layout.Item>
                        <Layout.Item css={itemCss} col={1}>
                            Результаты импорта
                        </Layout.Item>
                        <Layout.Item css={itemCss} col={1}>
                            <Badge text={importMock.result} type={STATUSES.SUCCESS} />
                        </Layout.Item>
                    </Layout>
                </Block.Body>
            </Block>
        </main>
    );
};

export default ImportDetail;
