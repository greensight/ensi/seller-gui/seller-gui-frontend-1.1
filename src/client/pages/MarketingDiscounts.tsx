import React, { useState, useMemo } from 'react';
import { Link } from 'react-router-dom';
import Form from '@standart/Form';
import Block from '@components/Block';
import typography from '@scripts/typography';
import { scale, Layout, Button } from '@greensight/gds';
import Table, { TableRowProps } from '@components/Table';
import MultiSelect from '@standart/MultiSelect';
import Select from '@standart/Select';
import { prepareForSelect } from '@scripts/helpers';
import DatepickerRange from '@standart/DatepickerRange';
import DatepickerStyles from '@standart/Datepicker/presets';
import Switcher from '@standart/Switcher';
import PageWrapper from '@components/PageWrapper';
import Tooltip from '@standart/Tooltip';
import TipIcon from '@svg/tokens/small/tooltip/tip.svg';
import { getRandomItem } from '@scripts/mock';
import useSelectedRowsData from '@scripts/useSelectedRowsData';
import Popup from '@standart/Popup';
import TrashIcon from '@svg/tokens/small/trash.svg';
import EditIcon from '@svg/tokens/small/edit.svg';
import { MARKETING_DISCOUNTS, MARKETING_DISCOUNTS_STATUSES } from '@scripts/data/different';

const statuses = prepareForSelect(MARKETING_DISCOUNTS);
const roles = prepareForSelect(['Все', 'Профессионал', 'Реферальный партнер']);
const discounts = prepareForSelect(MARKETING_DISCOUNTS_STATUSES);

const COLUMNS = [
    {
        Header: 'ID',
        accessor: 'id',
    },
    {
        Header: 'Дата создания',
        accessor: 'date',
        getProps: () => ({ type: 'date' }),
    },
    {
        Header: 'Название',
        accessor: 'name',
        getProps: () => ({ type: 'link' }),
    },
    {
        Header: 'Скидка',
        accessor: 'discount',
        getProps: () => ({ type: 'double' }),
    },
    {
        Header: 'Период действия',
        accessor: 'period',
        getProps: () => ({ type: 'dateRange' }),
    },
    {
        Header: 'Инициатор',
        accessor: 'initiator',
    },
    {
        Header: 'Статус',
        accessor: 'status',
        getProps: () => ({ type: 'status' }),
    },
];

const Filters = () => {
    const [moreFilters, setMoreFilters] = useState(false);

    return (
        <Block css={{ marginBottom: scale(3) }}>
            <DatepickerStyles />
            <Form
                initialValues={{
                    ID: '',
                    name: '',
                    status: [],
                    discounts: [],
                    role: roles[0].label,
                    initiator: '',
                    author: '',
                    datepickerCreation: [],
                    datepickerRange: [],
                    exactDiscountStartDate: false,
                    exactDiscountEndDate: false,
                    indefiniteDate: false,
                }}
                onSubmit={values => {
                    console.log(values);
                }}
            >
                <Block.Body>
                    <Layout cols={12}>
                        <Layout.Item col={4}>
                            <Form.FastField name="ID" label="ID" />
                        </Layout.Item>
                        <Layout.Item col={4}>
                            <Form.FastField name="name" label="Название" />
                        </Layout.Item>
                        <Layout.Item col={4}>
                            <Form.FastField name="status" label="Статус">
                                <MultiSelect options={statuses} />
                            </Form.FastField>
                        </Layout.Item>
                        {moreFilters ? (
                            <>
                                <Layout.Item col={4}>
                                    <Form.FastField name="discounts" label="Скидка на">
                                        <MultiSelect options={discounts} />
                                    </Form.FastField>
                                </Layout.Item>
                                <Layout.Item col={4}>
                                    <Form.FastField name="role" label="Роль">
                                        <Select items={roles} defaultValue={roles[0].label} placeholder="" />
                                    </Form.FastField>
                                </Layout.Item>
                                <Layout.Item col={4}>
                                    <Form.FastField name="initiator" label="Инициатор" />
                                </Layout.Item>
                                <Layout.Item col={6}>
                                    <Form.Field name="datepickerCreation" label="Дата создания">
                                        <DatepickerRange />
                                    </Form.Field>
                                </Layout.Item>
                                <Layout.Item col={6}>
                                    <Form.Field name="datepickerRange" label="Период действия скидки">
                                        <DatepickerRange />
                                    </Form.Field>
                                </Layout.Item>
                                <Layout.Item col={3}>
                                    <Form.FastField name="exactDiscountStartDate">
                                        <Switcher>
                                            <span>
                                                Точная дата начала скидки{' '}
                                                <Tooltip content="Искать точное совпадание с датой начала скидки">
                                                    <button type="button" css={{ verticalAlign: 'middle' }}>
                                                        <TipIcon />
                                                    </button>
                                                </Tooltip>
                                            </span>
                                        </Switcher>
                                    </Form.FastField>
                                </Layout.Item>
                                <Layout.Item col={3}>
                                    <Form.FastField name="exactDiscountEndDate">
                                        <Switcher>
                                            <span>
                                                Точная дата окончания скидки{' '}
                                                <Tooltip content="Искать точное совпадание с датой окончания скидки">
                                                    <button type="button" css={{ verticalAlign: 'middle' }}>
                                                        <TipIcon />
                                                    </button>
                                                </Tooltip>
                                            </span>
                                        </Switcher>
                                    </Form.FastField>
                                </Layout.Item>
                                <Layout.Item col="7/10">
                                    <Form.FastField name="indefiniteDate">
                                        <Switcher>Бессрочная скидка</Switcher>
                                    </Form.FastField>
                                </Layout.Item>
                            </>
                        ) : null}
                    </Layout>
                </Block.Body>
                <Block.Footer>
                    <div css={typography('bodySm')}>
                        <Button theme="outline" size="sm" onClick={() => setMoreFilters(!moreFilters)}>
                            {moreFilters ? 'Меньше' : 'Больше'} фильтров
                        </Button>
                    </div>
                    <div>
                        <Form.Reset size="sm" theme="secondary" type="button">
                            Сбросить
                        </Form.Reset>
                        <Button size="sm" theme="primary" css={{ marginLeft: scale(2) }} type="submit">
                            Применить
                        </Button>
                    </div>
                </Block.Footer>
            </Form>
        </Block>
    );
};

const newDiscount = (id: number) => {
    return {
        id: `20${id}`,
        date: new Date(),
        name: getRandomItem([
            ['Резинка и стайер', `marketing/discounts/20${id}`],
            ['Вентилятор и что-то еще', `marketing/discounts/20${id}`],
        ]),
        discount: ['10% на', 'Бандл из товаров'],
        period: [new Date('2020-12-24'), new Date()],
        initiator: 'Маркетплейс',
        status: 'Активна',
    };
};
const makeDiscounts = (len: number) => [...Array(len).keys()].map(el => newDiscount(el));

const MarketingDiscounts = () => {
    const data = useMemo(() => makeDiscounts(10), []);
    const [ids, setIds, selectedRows] = useSelectedRowsData<TableRowProps>(data);

    const [isDeleteOpen, setIsDeleteOpen] = useState(false);
    const [isChangeOpen, setIsChangeOpen] = useState(false);

    return (
        <>
            <PageWrapper h1={'Скидки'}>
                <Filters />

                <div css={{ display: 'flex', marginBottom: scale(3) }}>
                    <Button
                        size="sm"
                        theme="primary"
                        as={Link}
                        to={(location: Record<string, any>) => `${location.pathname}/create`}
                        css={{ marginRight: scale(1) }}
                    >
                        Создать скидку
                    </Button>
                    {ids.length > 0 ? (
                        <>
                            <Button
                                size="sm"
                                theme="primary"
                                css={{ marginRight: scale(1) }}
                                onClick={() => setIsDeleteOpen(true)}
                                Icon={TrashIcon}
                            >
                                Удалить {`скидк${ids.length > 1 ? 'и' : 'у'}`}
                            </Button>
                            <Button
                                size="sm"
                                theme="primary"
                                css={{ marginRight: scale(1) }}
                                onClick={() => setIsChangeOpen(true)}
                                Icon={EditIcon}
                            >
                                Отправить на согласование {`скид${ids.length > 1 ? 'ки' : 'ку'}`}
                            </Button>
                        </>
                    ) : null}
                </div>

                <Block>
                    <Block.Body>
                        <Table columns={COLUMNS} data={data} onRowSelect={setIds} />
                    </Block.Body>
                </Block>
            </PageWrapper>
            <Popup
                isOpen={isDeleteOpen}
                onRequestClose={() => setIsDeleteOpen(false)}
                title="Вы уверены, что хотите удалить следующие скидки?"
                popupCss={{ minWidth: scale(50) }}
            >
                <ul css={{ marginBottom: scale(2) }}>
                    {selectedRows.map(r => (
                        <li key={r.id} css={{ marginBottom: scale(1, true) }}>
                            #{r.id} – {r.name[0]}
                        </li>
                    ))}
                </ul>
                <Button size="sm">Удалить</Button>
            </Popup>
            <Popup
                isOpen={isChangeOpen}
                onRequestClose={() => setIsChangeOpen(false)}
                title="Обновление статуса"
                popupCss={{ minWidth: scale(50) }}
            >
                <ul css={{ marginBottom: scale(2) }}>
                    {selectedRows.map(r => (
                        <li key={r.id} css={{ marginBottom: scale(1, true) }}>
                            #{r.id} – {r.name[0]}
                        </li>
                    ))}
                </ul>
                <Form
                    initialValues={{ status: '' }}
                    onSubmit={() => {
                        console.log('send');
                    }}
                >
                    <Form.Field name="status" label="Новый статус" css={{ marginBottom: scale(2) }}>
                        <Select items={statuses} placeholder="" />
                    </Form.Field>
                    <Button size="sm" type="submit">
                        Изменить статус
                    </Button>
                </Form>
            </Popup>
        </>
    );
};

export default MarketingDiscounts;
