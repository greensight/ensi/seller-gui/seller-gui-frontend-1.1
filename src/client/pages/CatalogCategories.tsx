import React from 'react';
import { Layout, scale } from '@greensight/gds';

import Block from '@components/Block';
import PageWrapper from '@components/PageWrapper';

import typography from '@scripts/typography';
import { Categories, categoriesData } from '@scripts/mock';

interface CategoriesListProps {
    categories: Categories[];
    isFirstLvl?: boolean;
}

const CategoriesList = ({ categories, isFirstLvl }: CategoriesListProps) => {
    return (
        <ul css={{ ...typography('bodyMd') }}>
            {categories.map(category => (
                <li key={category.id} css={{ ...(!isFirstLvl && { marginLeft: scale(3) }) }}>
                    <p
                        css={{
                            position: 'relative',
                            paddingLeft: scale(3),
                            marginBottom: scale(1, true),
                            '&::before': {
                                ...(category.subCategories && { content: '"—"', position: 'absolute', left: 0 }),
                            },
                            ...(isFirstLvl && { ...typography('bodyMdBold') }),
                        }}
                    >
                        {category.name}
                    </p>
                    {category.subCategories && <CategoriesList categories={category.subCategories} />}
                </li>
            ))}
        </ul>
    );
};

const CatalogCategories = () => {
    return (
        <PageWrapper h1="Категории">
            <Layout cols={2}>
                <Layout.Item col={1}>
                    <Block>
                        <Block.Body>
                            <CategoriesList categories={categoriesData} isFirstLvl />
                        </Block.Body>
                    </Block>
                </Layout.Item>
            </Layout>
        </PageWrapper>
    );
};

export default CatalogCategories;
