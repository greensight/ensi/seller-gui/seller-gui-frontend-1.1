import React, { useMemo } from 'react';
import { Layout, Button, scale } from '@greensight/gds';
import * as Yup from 'yup';
import { useFormikContext, FieldArray } from 'formik';
import { nanoid } from 'nanoid';

import Block from '@components/Block';
import PageWrapper from '@components/PageWrapper';
import FilePond from '@components/FilePond';
import FilePondStyles from '@components/FilePond/FilePondStyles';
import Editor from '@components/Editor';

import Form from '@standart/Form';
import Switcher from '@standart/Switcher';
import Select from '@standart/Select';
import Datepicker from '@standart/Datepicker';
import DatepickerStyles from '@standart/Datepicker/presets';

import TrashIcon from '@svg/tokens/small/trash.svg';
import PlusIcon from '@svg/plus.svg';

import typography from '@scripts/typography';
import { getYouTubeVideoId, prepareForSelect } from '@scripts/helpers';
import { errorMessages } from '@scripts/errorMessages';
import FormField from '@standart/Form/Field';

const brandsForSelect = prepareForSelect(['Auchan', 'Leroy Merlin', 'Loreal', 'Utkonos']);
const categoryForSelect = prepareForSelect(['Товары для дома', 'Продукты', 'Косметика', 'Шампуни']);
const statusForSelect = prepareForSelect([
    'Недоступен к продаже',
    'В продаже',
    'Снято с продажи',
    'Доступен к продаже',
]);

const initialValues = {
    name: '',
    vendorCode: '',
    isNew: false,
    brand: '',
    category: '',
    status: '',
    price: '',
    width: 0,
    height: 0,
    depth: 0,
    weight: 0,
    isSpecialPacking: false,
    specialPacking: '',
    isSpecialStorage: false,
    specialStorage: '',
    isFragile: false,
    returnDaysCount: '',
    isFlammable: false,
    hasBattery: false,
    isGas: false,
    certName: '',
    certDate: '',
    restLunevo: 0,
    restAlabushevo: 0,
    styerSize: '',
    characteristics: [],
    description: '',
    youtube: '',
};

const Сharacteristics = () => {
    const {
        values: { characteristics },
    } = useFormikContext<{ characteristics: { value: string; id: string }[] }>();
    return (
        <>
            <Layout cols={2}>
                <Layout.Item col={1}>
                    <FormField name="styerSize" css={{ marginBottom: scale(2) }} label="Размер стайлера">
                        <Select items={prepareForSelect(['Маленький', 'Средний', 'Большой'])} />
                    </FormField>
                </Layout.Item>
                <Layout.Item col={1}>
                    <FieldArray
                        name="characteristics"
                        render={({ remove, push }) => (
                            <>
                                <div css={{ display: 'flex', alignItems: 'center', marginBottom: scale(2) }}>
                                    <span>Материал</span>
                                    <Button
                                        theme="outline"
                                        size="sm"
                                        title="Добавить"
                                        onClick={() => push({ value: '', id: nanoid() })}
                                        Icon={PlusIcon}
                                        hidden
                                        css={{ marginLeft: scale(2) }}
                                    >
                                        Удалить
                                    </Button>
                                </div>
                                <ul>
                                    {characteristics.map((char, index) => (
                                        <li
                                            key={char.id}
                                            css={{ marginBottom: scale(2), display: 'flex', alignItems: 'center' }}
                                        >
                                            <Form.Field
                                                name={`characteristics[${index}].value`}
                                                css={{ marginRight: scale(1) }}
                                            />
                                            <Button
                                                theme="outline"
                                                size="sm"
                                                title="Удалить"
                                                onClick={() => remove(index)}
                                                Icon={TrashIcon}
                                                hidden
                                            >
                                                Удалить
                                            </Button>
                                        </li>
                                    ))}
                                </ul>
                            </>
                        )}
                    />
                </Layout.Item>
            </Layout>
        </>
    );
};

const SpecialFields = () => {
    const {
        values: { isSpecialPacking, isSpecialStorage },
    } = useFormikContext();

    return (
        <>
            <Layout.Item col={1}>
                <Form.FastField name="isSpecialPacking" css={{ marginBottom: scale(1) }}>
                    <Switcher>Особая упаковка</Switcher>
                </Form.FastField>
                {isSpecialPacking ? <Form.FastField name="specialPacking" /> : null}
            </Layout.Item>
            <Layout.Item col={1}>
                <Form.FastField name="isSpecialStorage" css={{ marginBottom: scale(1) }}>
                    <Switcher>Особые условия хранения</Switcher>
                </Form.FastField>
                {isSpecialStorage ? <Form.FastField name="specialStorage" /> : null}
            </Layout.Item>
        </>
    );
};

const YouTubeUrl = () => {
    const {
        values: { youtube },
    } = useFormikContext();

    const videoId = useMemo(() => getYouTubeVideoId(youtube), [youtube]);
    return (
        <>
            <Form.FastField type="url" name="youtube" label="Ссылка на youtube" css={{ marginBottom: scale(2) }} />
            {videoId ? (
                <iframe
                    width="576"
                    height="324"
                    src={`https://www.youtube.com/embed/${videoId}`}
                    frameBorder="0"
                    title="Embedded youtube"
                />
            ) : null}
        </>
    );
};

const CatalogProductsCreate = () => {
    return (
        <Form
            initialValues={initialValues}
            onSubmit={vals => console.log('submit', vals)}
            validationSchema={Yup.object().shape({
                name: Yup.string().required(errorMessages.required),
                vendorCode: Yup.string().required(errorMessages.required),
            })}
        >
            <DatepickerStyles />
            <FilePondStyles />
            <PageWrapper h1="Создание товара">
                <div css={{ display: 'flex' }}>
                    <Block css={{ maxWidth: scale(128) }}>
                        <Block.Body>
                            <Layout cols={2}>
                                <Layout.Item col={1}>
                                    <Form.FastField name="name" label="Название товара" />
                                </Layout.Item>
                                <Layout.Item col={1}>
                                    <Form.FastField name="vendorCode" label="Артикул" />
                                </Layout.Item>
                                <Layout.Item col={2}>
                                    <Form.FastField name="isNew" label="Новинка">
                                        <Switcher>Новинка</Switcher>
                                    </Form.FastField>
                                </Layout.Item>
                                <Layout.Item col={1}>
                                    <Form.FastField name="brand" label="Бренд">
                                        <Select items={brandsForSelect} />
                                    </Form.FastField>
                                </Layout.Item>
                                <Layout.Item col={1}>
                                    <Form.FastField name="brand" label="Категория">
                                        <Select items={categoryForSelect} />
                                    </Form.FastField>
                                </Layout.Item>
                            </Layout>
                        </Block.Body>
                        <Block.Hr />
                        <Block.Body>
                            <h2 css={{ ...typography('h2'), marginBottom: scale(2) }}>Цена и статус</h2>

                            <Layout cols={2}>
                                <Layout.Item col={1}>
                                    <Form.FastField name="status" label="Статус товара">
                                        <Select items={statusForSelect} />
                                    </Form.FastField>
                                </Layout.Item>
                                <Layout.Item col={1}>
                                    <Form.FastField type="number" name="price" label="Стоимость товара" />
                                </Layout.Item>
                            </Layout>
                        </Block.Body>
                        <Block.Hr />
                        <Block.Body>
                            <h2 css={{ ...typography('h2'), marginBottom: scale(2) }}>Условия хранения</h2>

                            <Layout cols={2}>
                                <Layout.Item col={1}>
                                    <Form.FastField name="width" label="Ширина, мм" type="number" />
                                </Layout.Item>
                                <Layout.Item col={1}>
                                    <Form.FastField name="height" label="Высота, мм" type="number" />
                                </Layout.Item>
                                <Layout.Item col={1}>
                                    <Form.FastField name="depth" label="Глубина, мм" type="number" />
                                </Layout.Item>
                                <Layout.Item col={1}>
                                    <Form.FastField name="height" label="Вес, мм" type="number" />
                                </Layout.Item>

                                <SpecialFields />

                                <Layout.Item col={2}>
                                    <Form.FastField name="returnDaysCount" label="Дней на возврат" type="number" />
                                </Layout.Item>

                                <Layout.Item col={1}>
                                    <Form.FastField name="isFragile">
                                        <Switcher>Хрупкий товар</Switcher>
                                    </Form.FastField>
                                </Layout.Item>

                                <Layout.Item col={1}>
                                    <Form.FastField name="isFlammable">
                                        <Switcher>Легковоспламеняющееся</Switcher>
                                    </Form.FastField>
                                </Layout.Item>
                                <Layout.Item col={1}>
                                    <Form.FastField name="hasBattery">
                                        <Switcher>В составе есть элемент питания</Switcher>
                                    </Form.FastField>
                                </Layout.Item>
                                <Layout.Item col={1}>
                                    <Form.FastField name="isGas">
                                        <Switcher>Газ</Switcher>
                                    </Form.FastField>
                                </Layout.Item>
                            </Layout>
                        </Block.Body>
                        <Block.Hr />
                        <Block.Body>
                            <h2 css={{ ...typography('h2'), marginBottom: scale(2) }}>Сертификат</h2>

                            <Layout cols={2}>
                                <Layout.Item col={1}>
                                    <Form.FastField name="certName" label="Название сертификата" />
                                </Layout.Item>
                                <Layout.Item col={1}>
                                    <Form.FastField name="certDate" label="Сертификат действителен до">
                                        <Datepicker />
                                    </Form.FastField>
                                </Layout.Item>
                                <Layout.Item col={2}>
                                    <FilePond maxFiles={1} />
                                </Layout.Item>
                            </Layout>
                        </Block.Body>
                        <Block.Hr />
                        <Block.Body>
                            <h2 css={{ ...typography('h2'), marginBottom: scale(2) }}>Остатки</h2>

                            <Layout cols={2}>
                                <Layout.Item col={1}>
                                    <Form.FastField name="restLunevo" label="Лунево" />
                                </Layout.Item>
                                <Layout.Item col={1}>
                                    <Form.FastField name="restAlabushevo" label="Алабушево" />
                                </Layout.Item>
                            </Layout>
                        </Block.Body>
                        <Block.Hr />
                        <Block.Body>
                            <h2 css={{ ...typography('h2'), marginBottom: scale(2) }}>Характеристики</h2>

                            <Сharacteristics />
                        </Block.Body>
                        <Block.Hr />
                        <Block.Body>
                            <h2 css={{ ...typography('h2'), marginBottom: scale(2) }}>Описание</h2>

                            <Form.Field name="description">
                                <Editor />
                            </Form.Field>
                        </Block.Body>
                        <Block.Hr />
                        <Block.Body>
                            <h2 css={{ ...typography('h2'), marginBottom: scale(2) }}>Изображения</h2>

                            <FilePond imageLayout />
                        </Block.Body>
                        <Block.Body>
                            <h2 css={{ ...typography('h2'), marginBottom: scale(2) }}>How to use</h2>

                            <YouTubeUrl />
                        </Block.Body>
                    </Block>
                    <div css={{ position: 'relative', marginLeft: scale(3) }}>
                        <Button type="submit" css={{ position: 'sticky', top: scale(3) }}>
                            Создать товар
                        </Button>
                    </div>
                </div>
            </PageWrapper>
        </Form>
    );
};

export default CatalogProductsCreate;
