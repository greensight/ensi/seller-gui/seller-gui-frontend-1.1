import React, { useMemo } from 'react';
import { useLocation } from 'react-router-dom';

import { Button, scale, useTheme, Layout } from '@greensight/gds';
import typography from '@scripts/typography';
import Table from '@components/Table';
import Block from '@components/Block';

import Pagination from '@standart/Pagination';
import ExportIcon from '@svg/tokens/small/export.svg';

const brands = ['Vikki&Lilli', 'Ikoo', 'Rowenta', "L'oreal"];

const brandGroupTableItem = (id: number, brand: string) => {
    return {
        id: `100${id}`,
        title: brand,
        logo: null,
        code: '001',
    };
};

const makeBrands = () => brands.map((el, index) => brandGroupTableItem(index, el));

const COLUMNS = [
    {
        Header: 'ID',
        accessor: 'id',
    },
    {
        Header: 'Логотип',
        accessor: 'logo',
        getProps: () => ({ type: 'photo' }),
    },
    {
        Header: 'Название',
        accessor: 'title',
    },
    {
        Header: 'Код',
        accessor: 'code',
    },
];

const BrandTable = () => {
    const { colors } = useTheme();

    const { pathname, search } = useLocation();
    const activePage = +(new URLSearchParams(search).get('page') || 1);
    const data = useMemo(() => makeBrands(), []);

    return (
        <Block>
            <Block.Body>
                <Table columns={COLUMNS} data={data} editRow={() => console.log('click')}>
                    <colgroup>
                        <col width="5%" />
                        <col width="10%" />
                        <col width="10%" />
                        <col width="30%" />
                        <col width="40%" />
                        <col width="5%" />
                    </colgroup>
                </Table>
                <Pagination url={pathname} activePage={activePage} pages={7} />
            </Block.Body>
        </Block>
    );
};

const Brands = () => {
    const { colors } = useTheme();

    return (
        <main css={{ flexGrow: 1, flexShrink: 1, padding: `${scale(2)}px ${scale(3)}px` }}>
            <h1 css={{ ...typography('h1'), marginBottom: scale(2) }}>Бренды</h1>

            <Button size="sm" css={{ marginBottom: scale(2) }} Icon={ExportIcon}>
                Скачать
            </Button>

            <BrandTable />
        </main>
    );
};

export default Brands;
