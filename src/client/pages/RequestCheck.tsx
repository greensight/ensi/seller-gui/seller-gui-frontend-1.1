import React, { useState, useMemo } from 'react';
import { useLocation } from 'react-router-dom';
import { Button, scale, useTheme, Layout } from '@greensight/gds';
import typography from '@scripts/typography';
import Table from '@components/Table';
import Block from '@components/Block';
import Form from '@standart/Form';
import MultiSelect from '@standart/MultiSelect';
import { makeRequesters } from '@scripts/mock';
import Pagination from '@standart/Pagination';
import { STATUSES } from '@scripts/data/different';
import DatepickerStyles from '@standart/Datepicker/presets';
import DatepickerRange from '@standart/DatepickerRange';
import Legend from '@standart/Legend';
import PageWrapper from '@components/PageWrapper';

const statuses = STATUSES.map(i => ({ label: i, value: i }));

const COLUMNS = [
    {
        Header: 'ID',
        accessor: 'id',
        getProps: () => ({ type: 'linkedID' }),
    },
    {
        Header: 'Автор',
        accessor: 'author',
    },
    {
        Header: 'Статус',
        accessor: 'status',
        getProps: () => ({ type: 'status' }),
    },
    {
        Header: 'Кол-во товаров',
        accessor: 'quantity',
    },
    {
        Header: 'Дата создания',
        accessor: 'created',
        getProps: () => ({ type: 'date' }),
    },
];

const RequestCheck = () => {
    const { colors } = useTheme();
    const { pathname, search } = useLocation();
    const activePage = +(new URLSearchParams(search).get('page') || 1);
    const data = useMemo(() => makeRequesters(10), []);
    const [ids, setIds] = useState<number[]>([]);
    return (
        <PageWrapper h1="Проверка товаров">
            <main>
                <Block css={{ marginBottom: scale(3) }}>
                    <Form
                        initialValues={{
                            offerID: '',
                            status: [],
                            datepickerRange: [null, null],
                        }}
                        onSubmit={values => {
                            console.log(values);
                        }}
                    >
                        <Block.Body>
                            <Layout cols={8}>
                                <Layout.Item col={1}>
                                    <Form.Field name="offerID" label="ID оффера" />
                                </Layout.Item>
                                <Layout.Item col={3}>
                                    <Form.Field name="status" label="Статус">
                                        <MultiSelect options={statuses} />
                                    </Form.Field>
                                </Layout.Item>
                                <Layout.Item col={4}>
                                    <Form.Field name="datepickerRange">
                                        <DatepickerStyles />
                                        <Legend label="Введите дату" />
                                        <DatepickerRange />
                                    </Form.Field>
                                </Layout.Item>
                            </Layout>
                        </Block.Body>
                        <Block.Footer>
                            <div>
                                <Form.Reset size="sm" theme="secondary" type="button">
                                    Сбросить
                                </Form.Reset>
                                <Button size="sm" theme="primary" css={{ marginLeft: scale(2) }} type="submit">
                                    Применить
                                </Button>
                            </div>
                        </Block.Footer>
                    </Form>
                </Block>

                <Block>
                    <Block.Body>
                        <Table
                            columns={COLUMNS}
                            data={data}
                            editRow={row => console.log('rowdata', row)}
                            onRowSelect={setIds}
                        />
                        <Pagination url={pathname} activePage={activePage} pages={7} css={{ marginTop: scale(2) }} />
                    </Block.Body>
                </Block>
            </main>
        </PageWrapper>
    );
};

export default RequestCheck;
