import React, { useState, useMemo, useCallback } from 'react';
import { Button, scale, useTheme, Layout } from '@greensight/gds';
import { Helmet } from 'react-helmet-async';
import typography from '@scripts/typography';
import Block from '@components/Block';
import Badge from '@components/Badge';
import { STATUSES } from '@scripts/enums';
import Tabs from '@standart/Tabs';
import Categories from './Categories';
import Content from './Content';
import History from './History';
import Marketing from './Marketing';
import MasterData from './MasterData';
import Offers from './Offers';
import Orders from './Orders';
import StoreAndDelivery from './StoreAndDelivery';
import Popup from '@standart/Popup';
import Form from '@standart/Form';
import Select from '@standart/Select';
import PageWrapper from '@components/PageWrapper';

const productCharachteristics = [
    { name: 'ID:', value: 523 },
    { name: 'Артикул:', value: '444002' },
    { name: 'Текущая цена товара на витрине:', value: '259 руб' },
    { name: 'Текущий остаток товара на витрине:', value: '315 шт' },
    { name: 'Дата создания товара:', value: '2020-10-13 12:16:30' },
    { name: 'Дата последнего обновления товара:', value: '2021-03-04 10:48:20' },
];

const Product = () => {
    const { colors } = useTheme();
    const [isChangeStatusOpen, setIsChangeStatusOpen] = useState(false);

    return (
        <>
            <PageWrapper h1="Резинка для волос женская и детская, 3 штуки, очень хорошая">
                <>
                    <div css={{ display: 'inline-block', marginBottom: scale(2) }}>
                        <Block>
                            <Block.Body css={{ display: 'flex' }}>
                                <div css={{ marginRight: scale(4) }}>
                                    <Badge
                                        text="Согласовано"
                                        type={STATUSES.SUCCESS}
                                        css={{ marginBottom: scale(2) }}
                                    />
                                    <table css={{ margin: `${scale(2)}px 0` }}>
                                        <tbody>
                                            {productCharachteristics &&
                                                productCharachteristics.map(({ name, value }) => (
                                                    <tr key={name}>
                                                        <th css={{ textAlign: 'left', paddingRight: scale(2) }}>
                                                            {name}
                                                        </th>
                                                        <td css={{ textAlign: 'right' }}>{value}</td>
                                                    </tr>
                                                ))}
                                        </tbody>
                                    </table>
                                    <Button theme="outline" size="sm" onClick={() => setIsChangeStatusOpen(true)}>
                                        Изменить статус
                                    </Button>
                                </div>

                                <img src="https://placehold.it/200x200" alt="" />
                            </Block.Body>
                        </Block>
                    </div>
                    <Tabs>
                        <Tabs.List>
                            <Tabs.Tab>Мастер-данные</Tabs.Tab>
                            <Tabs.Tab>Хранение и доставка</Tabs.Tab>
                            <Tabs.Tab>Контент</Tabs.Tab>
                            <Tabs.Tab>Категории</Tabs.Tab>
                            <Tabs.Tab>Предложения</Tabs.Tab>
                            <Tabs.Tab>В заказах</Tabs.Tab>
                            <Tabs.Tab>История</Tabs.Tab>
                            <Tabs.Tab>Маркетинг</Tabs.Tab>
                        </Tabs.List>
                        <Block>
                            <Block.Body>
                                <Tabs.Panel>
                                    <MasterData />
                                </Tabs.Panel>
                                <Tabs.Panel>
                                    <StoreAndDelivery />
                                </Tabs.Panel>
                                <Tabs.Panel>
                                    <Content />
                                </Tabs.Panel>
                                <Tabs.Panel>
                                    <Categories text="Резинки для волос" />
                                </Tabs.Panel>
                                <Tabs.Panel>
                                    <Offers />
                                </Tabs.Panel>
                                <Tabs.Panel>
                                    <Orders />
                                </Tabs.Panel>
                                <Tabs.Panel>
                                    <History />
                                </Tabs.Panel>
                                <Tabs.Panel>
                                    <Marketing />
                                </Tabs.Panel>
                            </Block.Body>
                        </Block>
                    </Tabs>
                </>
            </PageWrapper>

            <Popup
                isOpen={isChangeStatusOpen}
                onRequestClose={() => setIsChangeStatusOpen(false)}
                title="Изменить статус"
                popupCss={{
                    maxWidth: 'initial',
                    width: scale(70),
                }}
            >
                <Form
                    onSubmit={values => console.log(values)}
                    initialValues={{
                        status: null,
                    }}
                >
                    <Form.Field name="status" label="Статус проверки" css={{ marginBottom: scale(2) }}>
                        <Select
                            items={[
                                { label: 'Согласовано', value: 'agreed' },
                                { label: 'Не согласовано', value: 'notAgreed' },
                                { label: 'Отправлено', value: 'sent' },
                                { label: 'На рассмотрении', value: 'inProcess' },
                                { label: 'Отклонено', value: 'rejected' },
                            ]}
                        ></Select>
                    </Form.Field>
                    <div css={{ display: 'flex' }}>
                        <Form.Reset
                            size="sm"
                            theme="outline"
                            onClick={() => setIsChangeStatusOpen(false)}
                            css={{ marginRight: scale(2) }}
                        >
                            Отменить
                        </Form.Reset>
                        <Button type="submit" size="sm" theme="primary">
                            Сохранить
                        </Button>
                    </div>
                </Form>
            </Popup>
        </>
    );
};

export default Product;
