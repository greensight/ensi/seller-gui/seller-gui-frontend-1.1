import React, { FunctionComponent } from 'react';
import { Global, CSSObject } from '@emotion/core';

import { Button, scale, useTheme, Layout } from '@greensight/gds';
import typography from '@scripts/typography';

import Block from '@components/Block';

import Select from '@standart/Select';
import Form from '@standart/Form';
import Tabs from '@standart/Tabs';

const VariantGroupInfoPanel = () => {
    return (
        <Layout cols={3}>
            <Layout.Item col={2}>
                <Block css={{ marginBottom: scale(2) }}>
                    <Form
                        initialValues={{
                            name: 'Заколки для волос',
                        }}
                        onSubmit={values => {
                            console.log(values);
                        }}
                    >
                        <Block.Body>
                            <Layout cols={3}>
                                <Layout.Item col={1}>
                                    <div css={{ ...typography('h3') }}>Инфопанель</div>
                                </Layout.Item>

                                <Layout.Item col={1} />

                                <Layout.Item col={1}>
                                    <div
                                        css={{
                                            display: 'flex',
                                            justifyContent: 'flex-end',
                                        }}
                                    >
                                        <Button size="sm">Сохранить</Button>
                                        <Button size="sm" css={{ marginLeft: scale(2) }}>
                                            Отмена
                                        </Button>
                                        <Button size="sm" css={{ marginLeft: scale(2) }}>
                                            Удалить
                                        </Button>
                                    </div>
                                </Layout.Item>
                            </Layout>

                            <Layout cols={3}>
                                <Layout.Item col={1}>
                                    <Form.Field name="name" label="Название" />
                                    <div css={{ marginTop: scale(2) }}>
                                        <b>Дата создания:</b> 25.01.2021 17:20:34
                                    </div>
                                </Layout.Item>

                                <Layout.Item col={1}>
                                    <div
                                        css={{
                                            display: 'flex',
                                            flexDirection: 'column',
                                            justifyContent: 'space-between',
                                            height: '100%',
                                        }}
                                    >
                                        <span>
                                            <b>Продавец: </b> <a href="/">Тестовые товары</a>
                                        </span>

                                        <span>
                                            <b>Дата изменения</b> 25.01.2021 17:20:34
                                        </span>
                                    </div>
                                </Layout.Item>
                            </Layout>
                        </Block.Body>
                    </Form>
                </Block>
            </Layout.Item>

            <Layout.Item col={1}>
                <Block css={{ marginBottom: scale(2) }}>
                    <Block.Body>
                        <div>
                            <b>Кол-во характеристик:</b> 2
                        </div>

                        <div>
                            <b>Кол-во товаров:</b> 2
                        </div>
                    </Block.Body>
                </Block>
            </Layout.Item>
        </Layout>
    );
};

const VariantGroupProductForm = () => {
    return (
        <Form
            initialValues={{
                product: '',
            }}
            onSubmit={values => {
                console.log(values);
            }}
        >
            <div css={{ display: 'flex' }}>
                <Form.Field css={{ width: 300 }} name="product" />
                <Button size="sm" type="submit" css={{ marginLeft: scale(2), flex: 'none' }}>
                    Добавить товар
                </Button>
            </div>
        </Form>
    );
};

const VariantGroupCharacteristicsCard = () => {
    const { colors } = useTheme();

    const List: FunctionComponent = ({ children }) => {
        const listCss: CSSObject = {
            paddingLeft: scale(3),
        };

        return <ul css={listCss}>{children}</ul>;
    };

    const ListItem: FunctionComponent = ({ children }) => {
        const itemCss: CSSObject = {
            listStyle: 'initial',
            listStylePosition: 'inside',
        };

        return <li css={itemCss}>{children}</li>;
    };

    const rootCss: CSSObject = {
        padding: scale(3),
        border: '1px solid',
        borderColor: colors?.grey300,
        borderRadius: 8,

        '& + &': {
            marginTop: scale(3),
        },
    };

    const headerCss: CSSObject = {
        display: 'flex',
        justifyContent: 'space-between',
    };

    const listHeaderCss: CSSObject = {
        marginBottom: scale(1),
    };

    return (
        <div css={rootCss}>
            <div css={headerCss}>
                <div css={{ ...typography('h3') }}>Механизмы запирания/отпирания канала ствола (ID: 171)</div>
                <Button size="sm">Удалить</Button>
            </div>

            <div css={listHeaderCss}>
                <b>Используемые варианты значений</b>
            </div>

            <List>
                <ListItem>Вариант 1</ListItem>
                <ListItem>Вариант 2</ListItem>
                <ListItem>Вариант 3</ListItem>
                <ListItem>Вариант 4</ListItem>
            </List>
        </div>
    );
};

const VariantGroupCharacteristicsList = () => {
    const charTypes = [
        'Цвет',
        'Размер стайлера',
        'Материал',
        'Механизмы запирания/отпирания канала ствола',
        'Тест',
        'Новый',
        'Размер',
    ];

    const options = charTypes.map(t => ({ value: t, label: t }));

    const listCss: CSSObject = {
        paddingTop: scale(4),
    };

    return (
        <div>
            <Form
                initialValues={{
                    select: '',
                }}
                onSubmit={values => {
                    console.log(values);
                }}
            >
                <div css={{ display: 'flex' }}>
                    <Form.Field css={{ width: 300 }} name="select">
                        <Select items={options} selectedItem={options[0]} name="select" />
                    </Form.Field>

                    <Button size="sm" type="submit" css={{ marginLeft: scale(2), flex: 'none' }}>
                        Добавить характеристику
                    </Button>
                </div>
            </Form>

            <div css={listCss}>
                <VariantGroupCharacteristicsCard />
                <VariantGroupCharacteristicsCard />
                <VariantGroupCharacteristicsCard />
                <VariantGroupCharacteristicsCard />
                <VariantGroupCharacteristicsCard />
            </div>
        </div>
    );
};

const VariantGroupTabPanel = () => {
    const { colors } = useTheme();

    return (
        <>
            <Global
                styles={{
                    '.variant-group-tabs': {
                        '.tabs__list': {
                            display: 'flex',
                            borderBottom: '1px solid',
                            borderBottomColor: colors?.grey300,
                        },

                        '.tabs__tab': {
                            border: '1px solid',
                            borderColor: 'transparent',
                            padding: `12px ${scale(1)}px`,
                            borderTopRightRadius: 4,
                            borderTopLeftRadius: 4,

                            ':hover:not(&--selected):not(&--disabled)': {},

                            ':last-child': {
                                borderRightColor: 'transparent',
                                borderTopRightRadius: 4,
                                borderTopLeftRadius: 4,
                            },

                            ':first-of-type': {
                                borderTopRightRadius: 4,
                                borderTopLeftRadius: 4,
                            },

                            '&--selected': {
                                border: '1px solid',
                                borderColor: colors?.grey700,
                                borderBottomColor: 'transparent',
                                backgroundColor: colors?.grey300,
                                color: colors?.black,

                                '&:last-child': {
                                    borderRightColor: colors?.grey700,
                                },
                            },

                            '&--disabled': {
                                ':not(:first-of-type)': {},
                            },
                        },

                        '.tabs__panel': {
                            padding: `${scale(3)}px 0`,
                        },
                    },
                }}
            />

            <Block css={{ marginBottom: scale(2) }}>
                <Block.Body>
                    <Tabs defaultIndex={0} className="variant-group-tabs">
                        <Tabs.List>
                            <Tabs.Tab>Товары</Tabs.Tab>
                            <Tabs.Tab>Характеристики</Tabs.Tab>
                        </Tabs.List>

                        <Tabs.Panel>
                            <VariantGroupProductForm />
                        </Tabs.Panel>

                        <Tabs.Panel>
                            <VariantGroupCharacteristicsList />
                        </Tabs.Panel>
                    </Tabs>
                </Block.Body>
            </Block>
        </>
    );
};

const VariantGroup = () => {
    return (
        <main css={{ flexGrow: 1, flexShrink: 1, padding: `${scale(2)}px ${scale(3)}px` }}>
            <h1 css={{ ...typography('h1'), marginBottom: scale(2) }}>Товарная группа &quot;Заколки для волос&quot;</h1>

            <VariantGroupInfoPanel />
            <VariantGroupTabPanel />
        </main>
    );
};

export default VariantGroup;
