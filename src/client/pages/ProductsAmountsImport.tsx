import React from 'react';
import PageWrapper from '@components/PageWrapper';
import { Layout, scale, useTheme, Button } from '@greensight/gds';
import DownloadIcon from '@svg/tokens/small/export.svg';
import UploadIcon from '@svg/tokens/small/arrowUp.svg';
import PlusIcon from '@svg/plus.svg';
import * as Yup from 'yup';
import Radio from '@standart/Radio';
import Form from '@standart/Form';
import FilePond from '@components/FilePond';
import FilePondStyles from '@components/FilePond/FilePondStyles';
import Block from '@components/Block';

const ProductsAmountsImport = () => {
    const { colors } = useTheme();
    const onSubmit = () => {
        console.log('submit');
    };

    return (
        <PageWrapper h1={'Импорт остатков'}>
            <Form
                onSubmit={onSubmit}
                initialValues={{ importMode: 'changes' }}
                validationSchema={Yup.object().shape({
                    importMode: Yup.string().required('Обязательное поле'),
                })}
            >
                <Layout cols={3}>
                    <Layout.Item col={1}>
                        <Block>
                            <Block.Body>
                                <p css={{ marginBottom: scale(2), fontWeight: 'bold' }}>
                                    Режим импорта остатков по товарам
                                </p>
                                <Form.Field name="importMode">
                                    <Radio legend={'Выберите вариант'} name={'importMode'} isHiddenLegend={true}>
                                        <Radio.Item value="changes" checked>
                                            Только изменения
                                        </Radio.Item>
                                        <Radio.Item value="all">
                                            Все{' '}
                                            <span css={{ color: colors?.danger }}>
                                                (остатки товаров на складах, которых нет в файле, будут удалены!)
                                            </span>
                                        </Radio.Item>
                                    </Radio>
                                </Form.Field>
                            </Block.Body>
                        </Block>
                    </Layout.Item>
                    <Layout.Item col={1}>
                        <Block>
                            <Block.Body css={{ textAlign: 'right' }}>
                                <FilePondStyles />
                                <FilePond maxFileSize="10MB" maxTotalFileSize="100MB" />
                                <Button type="submit" theme={'primary'} size={'sm'} Icon={UploadIcon} iconAfter>
                                    Загрузить
                                </Button>
                            </Block.Body>
                        </Block>
                    </Layout.Item>
                    <Layout.Item col={1} css={{ textAlign: 'right' }}>
                        <Block>
                            <Block.Body>
                                <Button theme={'primary'} size={'sm'} Icon={DownloadIcon} iconAfter>
                                    Скачать шаблон
                                </Button>
                            </Block.Body>
                        </Block>
                    </Layout.Item>
                </Layout>
            </Form>
        </PageWrapper>
    );
};

export default ProductsAmountsImport;
