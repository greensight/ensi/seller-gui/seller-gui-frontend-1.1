import React, { useState, useMemo } from 'react';
import { useLocation } from 'react-router-dom';

import Block from '@components/Block';
import MultiSelect from '@standart/MultiSelect';
import Table from '@components/Table';
import headerWithTooltip from '@components/Table/HeaderWithTooltip';

import Form from '@standart/Form';
import Pagination from '@standart/Pagination';
import Datepicker from '@standart/Datepicker';

import { Button, scale, useTheme, Layout } from '@greensight/gds';

import typography from '@scripts/typography';
import { sellers } from '@scripts/mock';

const statuses = [
    'Оформлен',
    'Ожидает проверки АОЗ',
    'Проверка АОЗ',
    'Ожидает подтверждение Продавцом',
    'В обработке',
    'Передан на доставку',
    'В процессе доставки',
    'Находится в пункте выдачи',
    'Доставлен',
    'Оформлен возврат',
    'Предзаказ: ожидаем поступление товара',
];
const brands = ['Vikki&Lilli', 'Ikoo', 'Rowenta', "L'oreal"];
const paymentMethods = ['Онлайн'];
const stocks = [
    '124460, г Москва, г Зеленоград, р-н Силино, к 1206А',
    '124365, г Москва, г Зеленоград, р-н Крюково, ул Андреевка, д 12',
    '124460, г Москва, г Зеленоград, р-н Силино, к 1206А',
    '124460, г Москва, г Зеленоград, р-н Силино, поселок Алабушево, д 12',
    '601625, Владимирская обл, Александровский р-н, деревня Лунево, ул Центральная, д 1',
    '141580, Московская обл, г Солнечногорск, поселок Лунёво, д 1',
    '124482, г Москва, г Зеленоград, р-н Савелки, к 305',
    '141580, Московская обл, г Солнечногорск, поселок Лунёво, ул Гаражная, д 8',
    '124482, г Москва, г Зеленоград, р-н Савелки, Центральный пр-кт, к 305',
    '141532, Московская обл, г Солнечногорск, деревня Радумля, мкр Поварово-2, д 11',
    '155555, Ивановская обл, Приволжский р-н, село Мелехово, д 12',
];
const deliveryTypes = ['Несколькими доставками', 'Одной доставкой'];
const logisticsOperators = ['B2Cpl', 'Boxberry', 'СДЭК'];
const simpleAnswers = ['Не выбрано', 'Нет', 'Да'];
const confirmTypes = ['SMS', 'Звонок оператора'];
const paymentStatuses = ['Холдирование', 'Не оплачено', 'Оплачено', 'Просрочено'];
const deliveryCities = ['г Тверь', 'г Зеленоград', 'г Солнечногорск', 'г Курган', 'г Домодедово', 'г Казань'];

const STATUSES = statuses.map(i => ({ label: i, value: i }));
const BRANDS = brands.map(i => ({ label: i, value: i }));
const SELLERS = sellers.map(i => ({ label: i, value: i }));
const PAYMENT_METHODS = paymentMethods.map(i => ({ label: i, value: i }));
const STOCKS = stocks.map(i => ({ label: i, value: i }));
const DELIVERY_TYPES = deliveryTypes.map(i => ({ label: i, value: i }));
const LOGISTICS_OPERATORS = logisticsOperators.map(i => ({ label: i, value: i }));
const SIMPLE_ANSWERS = simpleAnswers.map(i => ({ label: i, value: i }));
const CONFIRMATION_TYPES = confirmTypes.map(i => ({ label: i, value: i }));

const getRandomItem = (arr: any[]) => arr[Math.floor(Math.random() * arr.length)];

const orderListTableItem = (num: number) => {
    return {
        id: `10000${num}`,
        date: new Date(),
        status: getRandomItem(statuses),
        confirmType: getRandomItem(confirmTypes),
        clientInfo: 'Резинкин Петр Петрович +79035299909',
        itemsCost: '210 руб',
        deliveryCost: '10 руб',
        orderCost: '220 руб',
        paymentMethod: getRandomItem(paymentMethods),
        paymentStatus: getRandomItem(paymentStatuses),
        logisticOperator: getRandomItem(logisticsOperators),
        deliveryCity: getRandomItem(deliveryCities),
        departAmount: Math.floor(Math.random() * 8 + 1),
        psd: new Date(),
        pdd: new Date(),
        orderSource: ' ',
    };
};

const makeOrders = (len: number) => [...Array(len).keys()].map(el => orderListTableItem(el));

const OrderListFilter = () => {
    const { colors } = useTheme();
    const [moreFilters, setMoreFilters] = useState(true);

    return (
        <Block css={{ marginBottom: scale(2) }}>
            <Form
                initialValues={{
                    id: '',
                    seller: [],
                    code: '',
                    article: '',
                    brand: [],
                    category: [],
                    created: '',
                }}
                onSubmit={values => {
                    console.log(values);
                }}
            >
                <Block.Body>
                    <Layout cols={12}>
                        <Layout.Item col={2}>
                            <Form.Field name="orderNumber" label="№ заказа" />
                        </Layout.Item>

                        <Layout.Item col={5}>
                            <Form.Field name="userInfo" label="ФИО, e-mail или телефон покупателя" />
                        </Layout.Item>

                        <Layout.Item col={3}>
                            <Form.Field name="date" label="Период заказа" />
                        </Layout.Item>

                        <Layout.Item col={2}>
                            <Form.Field name="status" label="Статус">
                                <MultiSelect options={STATUSES} />
                            </Form.Field>
                        </Layout.Item>

                        {moreFilters && (
                            <>
                                <Layout.Item col={12}>
                                    <Layout cols={4}>
                                        <Layout.Item col={1}>
                                            <Form.Field name="sumFrom" label="Сумма заказа" />
                                        </Layout.Item>

                                        <Layout.Item col={1} align="end">
                                            <Form.Field name="sumTo" label="" />
                                        </Layout.Item>

                                        <Layout.Item col={1}>
                                            <Form.Field name="code" label="Код товара из ERP" />
                                        </Layout.Item>

                                        <Layout.Item col={1}>
                                            <Form.Field name="vendorCode" label="Артикул" />
                                        </Layout.Item>

                                        <Layout.Item col={1}>
                                            <Form.Field name="brand" label="Бренд">
                                                <MultiSelect options={BRANDS} />
                                            </Form.Field>
                                        </Layout.Item>

                                        <Layout.Item col={1}>
                                            <Form.Field name="seller" label="Продавец">
                                                <MultiSelect options={SELLERS} />
                                            </Form.Field>
                                        </Layout.Item>

                                        <Layout.Item col={1}>
                                            <Form.Field name="paymentMethod" label="Способ оплаты">
                                                <MultiSelect options={PAYMENT_METHODS} />
                                            </Form.Field>
                                        </Layout.Item>

                                        <Layout.Item col={1}>
                                            <Form.Field name="stock" label="Склад отгрузки">
                                                <MultiSelect options={STOCKS} />
                                            </Form.Field>
                                        </Layout.Item>
                                    </Layout>
                                </Layout.Item>
                                <Layout.Item col={12}>
                                    <Layout cols={6}>
                                        <Layout.Item col={1}>
                                            <Form.Field name="deliveryType" label="Тип доставки">
                                                <MultiSelect options={DELIVERY_TYPES} />
                                            </Form.Field>
                                        </Layout.Item>

                                        <Layout.Item col={1}>
                                            <Form.Field name="logisticsOperator" label="Логистический оператор">
                                                <MultiSelect options={LOGISTICS_OPERATORS} />
                                            </Form.Field>
                                        </Layout.Item>

                                        <Layout.Item col={2}>
                                            <Form.Field name="deliveryCity" label="Город доставки" />
                                        </Layout.Item>

                                        <Layout.Item col={1}>
                                            <Form.Field name="psd" label="PSD" />
                                        </Layout.Item>

                                        <Layout.Item col={1}>
                                            <Form.Field name="pdd" label="PDD" />
                                        </Layout.Item>

                                        <Layout.Item col={1}>
                                            <Form.Field name="declined" label="Отменен">
                                                <MultiSelect options={SIMPLE_ANSWERS} isMulti={false} />
                                            </Form.Field>
                                        </Layout.Item>

                                        <Layout.Item col={1}>
                                            <Form.Field name="problem" label="Проблемный">
                                                <MultiSelect options={SIMPLE_ANSWERS} isMulti={false} />
                                            </Form.Field>
                                        </Layout.Item>

                                        <Layout.Item col={1}>
                                            <Form.Field name="check" label="Требует проверки">
                                                <MultiSelect options={SIMPLE_ANSWERS} isMulti={false} />
                                            </Form.Field>
                                        </Layout.Item>

                                        <Layout.Item col={1}>
                                            <Form.Field name="comfirm" label="Тип подтверждения">
                                                <MultiSelect options={CONFIRMATION_TYPES} />
                                            </Form.Field>
                                        </Layout.Item>

                                        <Layout.Item col={2}>
                                            <Form.Field name="comment" label="Комментарий менеджера" />
                                        </Layout.Item>
                                    </Layout>
                                </Layout.Item>
                            </>
                        )}
                    </Layout>
                </Block.Body>

                <Block.Footer>
                    <div css={typography('bodySm')}>
                        Всего заказов: 40.{' '}
                        <button
                            type="button"
                            css={{ color: colors?.primary, marginLeft: scale(2) }}
                            onClick={() => setMoreFilters(!moreFilters)}
                        >
                            {moreFilters ? 'Меньше' : 'Больше'} фильтров
                        </button>{' '}
                    </div>

                    <div>
                        <Form.Reset
                            size="sm"
                            theme="secondary"
                            type="button"
                            onClick={() => {
                                console.log('hoh');
                            }}
                        >
                            Сбросить
                        </Form.Reset>

                        <Button size="sm" theme="primary" css={{ marginLeft: scale(2) }} type="submit">
                            Применить
                        </Button>
                    </div>
                </Block.Footer>
            </Form>
        </Block>
    );
};

const COLUMNS = [
    {
        Header: '№ заказа',
        accessor: 'id',
        getProps: () => ({ type: 'linkedID' }),
    },
    {
        Header: 'Дата заказа',
        accessor: 'date',
        getProps: () => ({ type: 'date' }),
    },
    {
        Header: 'Статус',
        accessor: 'status',
        getProps: () => ({ type: 'status' }),
    },
    {
        Header: 'Тип подтверждения',
        accessor: 'confirmType',
    },
    {
        Header: 'Клиент',
        accessor: 'clientInfo',
    },
    {
        Header: 'Стоимость товаров',
        accessor: 'itemsCost',
    },
    {
        Header: 'Стоимость доставки',
        accessor: 'deliveryCost',
    },
    {
        Header: 'Стоимость заказа',
        accessor: 'orderCost',
    },
    {
        Header: 'Способ оплаты',
        accessor: 'paymentMethod',
    },
    {
        Header: 'Статус оплаты',
        accessor: 'paymentStatus',
        getProps: () => ({ type: 'status' }),
    },
    {
        Header: () =>
            headerWithTooltip({
                headerText: 'Логистический оператор',
                tooltipText: 'Логистический оператор на последней миле',
            }),
        accessor: 'logisticOperator',
    },
    {
        Header: 'Город доставки',
        accessor: 'deliveryCity',
    },
    {
        Header: 'Кол-во отправлений',
        accessor: 'departAmount',
    },
    {
        Header: () =>
            headerWithTooltip({
                headerText: 'PSD',
                tooltipText: 'По последнему отправлению последней доставки',
                tooltipPlacement: 'bottom',
            }),
        accessor: 'psd',
        getProps: () => ({ type: 'date' }),
    },
    {
        Header: () =>
            headerWithTooltip({
                headerText: 'PDD',
                tooltipText: 'По последней доставки',
                tooltipPlacement: 'bottom',
            }),
        accessor: 'pdd',
        getProps: () => ({ type: 'date' }),
    },
    {
        Header: 'Источник заказа',
        accessor: 'orderSource',
    },
];

const OrderListTable = () => {
    const { pathname, search } = useLocation();
    const activePage = +(new URLSearchParams(search).get('page') || 1);
    const data = useMemo(() => makeOrders(10), []);

    return (
        <Block>
            <Block.Body>
                <Table columns={COLUMNS} data={data} editRow={() => console.log('click')} />
                <Pagination url={pathname} activePage={activePage} pages={7} />
            </Block.Body>
        </Block>
    );
};

const OrderList = () => {
    return (
        <main css={{ flexGrow: 1, flexShrink: 1, padding: `${scale(2)}px ${scale(3)}px` }}>
            <h1 css={{ ...typography('h1'), marginBottom: scale(2) }}>Список заказов</h1>

            <OrderListFilter />
            <OrderListTable />
        </main>
    );
};

export default OrderList;
