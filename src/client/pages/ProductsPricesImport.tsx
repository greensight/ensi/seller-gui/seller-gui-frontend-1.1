import React from 'react';
import { useTheme, scale, Layout, Button } from '@greensight/gds';
import PageWrapper from '@components/PageWrapper';
import Block from '@components/Block';
import Form from '@standart/Form';
import * as Yup from 'yup';
import FilePond from '@components/FilePond';
import FilePondStyles from '@components/FilePond/FilePondStyles';
import ExportIcon from '@svg/tokens/small/export.svg';
import UploadIcon from '@svg/tokens/small/arrowUp.svg';

const ProductsPricesImport = () => {
    const { colors, shadows } = useTheme();

    const onSubmit = (values: any) => {
        console.log('submit', values);
    };

    return (
        <PageWrapper h1="Импорт цен">
            <div css={{ display: 'flex', justifyContent: 'space-between', marginBottom: scale(2) }}>
                <h3>Выберите файл для загрузки данных о ценах</h3>
                <Button size="sm" Icon={ExportIcon} iconAfter onClick={() => console.log('upload')}>
                    Скачать шаблон
                </Button>
            </div>

            <Layout cols={1}>
                <Layout.Item col={1}>
                    <Form onSubmit={onSubmit} initialValues={{ importFile: [] }}>
                        <Block css={{ maxWidth: 'fit-content' }}>
                            <Block.Body>
                                <div css={{ width: scale(63) }}>
                                    <Form.Field name="importFile">
                                        <FilePondStyles />
                                        <FilePond maxFileSize="10MB" maxTotalFileSize="100MB" />
                                    </Form.Field>
                                </div>
                                <Button type="submit" theme="primary" size="sm" Icon={UploadIcon} iconAfter>
                                    Загрузить
                                </Button>
                            </Block.Body>
                        </Block>
                    </Form>
                </Layout.Item>
                <Layout.Item col={1}>
                    <div
                        css={{
                            backgroundColor: colors?.warningBg,
                            padding: scale(2),
                            maxWidth: scale(63),
                            width: '100%',
                            lineHeight: '1.5',
                            boxShadow: shadows?.small,
                        }}
                    >
                        <h3 css={{ marginBottom: scale(2) }}>Внимание! Новые цены не появятся сразу на витрине!</h3>
                        <p>
                            Для товарных предложений из файла будут созданы заявки на изменения цен, которые отправятся
                            на согласование менеджеру платформы
                        </p>
                    </div>
                </Layout.Item>
            </Layout>
        </PageWrapper>
    );
};

export default ProductsPricesImport;
