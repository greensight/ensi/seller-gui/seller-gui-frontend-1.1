import React, { useState, useMemo } from 'react';
import { useParams } from 'react-router-dom';
import PageWrapper from '@components/PageWrapper';
import { CSSObject } from '@emotion/core';
import { format } from 'date-fns';
import { scale, useTheme, Layout } from '@greensight/gds';
import Table from '@components/Table';
import Block from '@components/Block';
import Badge from '@components/Badge';
import Tabs from '@standart/Tabs';
import typography from '@scripts/typography';
import { makePricesChanges } from '@scripts/mock';

const columns = [
    {
        Header: 'ID предложения продавца',
        accessor: 'offerId',
    },
    {
        Header: 'ID товара',
        accessor: 'productId',
    },
    {
        Header: 'Название товара',
        accessor: 'title',
        getProps: () => ({ type: 'link' }),
    },
    {
        Header: 'Старая цена предложения продавца',
        accessor: 'oldPrice',
        getProps: () => ({ type: 'price' }),
    },
    {
        Header: 'Новая цена предложения продавца',
        accessor: 'newPrice',
        getProps: () => ({ type: 'price' }),
    },
    {
        Header: 'Артикул',
        accessor: 'code',
    },
    {
        Header: 'Статус изменения цены',
        accessor: 'status',
        getProps: () => ({ type: 'status' }),
    },
    {
        Header: 'Комментарий по изменению цены',
        accessor: 'comment',
    },
];

const pricesChangesOffer = (id: number) => {
    return {
        offerId: `100${id}`,
        productId: 526,
        title: ['Стайлер для волос IKOO E-styler Pro Beluga Black', `/products/catalog/100${id}`],
        oldPrice: 0,
        newPrice: 8999,
        code: 292430,
        status: 'Изменена 13:57:48 2020-10-13',
        comment: '',
    };
};

const item = makePricesChanges(1)[0];

const PriceChangeItem = () => {
    const { id } = useParams<{ id: string }>();
    const { status, author, quantity, created } = item;
    const { colors } = useTheme();
    const dlStyles: CSSObject = { display: 'grid', gridTemplateColumns: "'1fr 1fr'" };
    const dtStyles: CSSObject = {
        padding: `${scale(1, true)}px ${scale(1)}px ${scale(1, true)}px 0`,
        borderBottom: `1px solid ${colors?.grey200}`,
        ...typography('bodySmBold'),
        ':last-of-type': { border: 'none' },
    };
    const ddStyles: CSSObject = {
        padding: `${scale(1, true)}px 0`,
        borderBottom: `1px solid ${colors?.grey200}`,
        ':last-of-type': { border: 'none' },
    };

    const makePricesChangesOffers = (len: number) => [...Array(len).keys()].map(el => pricesChangesOffer(el));

    const tableData = useMemo(() => makePricesChangesOffers(10), []);
    const tableColumns = useMemo(() => columns, []);
    return (
        <PageWrapper h1={`Заявка #${id} от ${format(new Date(created), 'dd.MM.yyyy')}`}>
            <main>
                <Layout cols={2} css={{ marginBottom: scale(3) }}>
                    <Layout.Item>
                        <Block>
                            <Block.Body>
                                <dl css={{ ...dlStyles, gridTemplateColumns: '150px 1fr' }}>
                                    <dt css={dtStyles}>Статус</dt>
                                    <dd css={ddStyles}>
                                        <Badge text={status} />
                                    </dd>
                                    <dt css={dtStyles}>ID</dt>
                                    <dd css={ddStyles}>{id}</dd>
                                    <dt css={dtStyles}>Автор</dt>
                                    <dd css={ddStyles}>{author[0]}</dd>
                                    <dt css={dtStyles}>Автор</dt>
                                    <dd css={ddStyles}>{quantity}&nbsp;ед. товара</dd>
                                    <dt css={dtStyles}>Дата создания</dt>
                                    <dd css={ddStyles}>{format(new Date(created), 'dd.MM.yyyy')}</dd>
                                </dl>
                            </Block.Body>
                        </Block>
                    </Layout.Item>
                </Layout>
                <Tabs>
                    <Tabs.List>
                        <Tabs.Tab>Предложения продавца</Tabs.Tab>
                    </Tabs.List>
                    <Block>
                        <Block.Body>
                            <Tabs.Panel>
                                <Table
                                    columns={tableColumns}
                                    data={tableData}
                                    needCheckboxesCol={false}
                                    needSettingsColumn={false}
                                    css={{ marginTop: scale(2) }}
                                />
                            </Tabs.Panel>
                        </Block.Body>
                    </Block>
                </Tabs>
            </main>
        </PageWrapper>
    );
};

export default PriceChangeItem;
