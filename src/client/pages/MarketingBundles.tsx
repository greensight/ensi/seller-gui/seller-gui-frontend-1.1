import React, { useState, useMemo } from 'react';
import { Link } from 'react-router-dom';
import { scale, Layout, Button } from '@greensight/gds';
import { FormikValues } from 'formik';

import Block from '@components/Block';
import Table from '@components/Table';
import PageWrapper from '@components/PageWrapper';

import Form from '@standart/Form';
import MultiSelect from '@standart/MultiSelect';
import Select from '@standart/Select';
import DatepickerRange from '@standart/DatepickerRange';
import DatepickerStyles from '@standart/Datepicker/presets';
import Switcher from '@standart/Switcher';
import Tooltip from '@standart/Tooltip';
import Popup from '@standart/Popup';

import { getRandomItem } from '@scripts/mock';
import useSelectedRowsData from '@scripts/useSelectedRowsData';
import typography from '@scripts/typography';
import { STATUSES } from '@scripts/data/different';
import { prepareForSelect } from '@scripts/helpers';
import { useURLHelper } from '@scripts/useURLHelper';

import TipIcon from '@svg/tokens/small/tooltip/tip.svg';
import TrashIcon from '@svg/tokens/small/trash.svg';
import EditIcon from '@svg/tokens/small/edit.svg';

const statuses = prepareForSelect(STATUSES);
const roles = prepareForSelect(['Все', 'Профессионал', 'Реферальный партнер']);
const discounts = prepareForSelect(['Бандл из товаров', 'Бандл из мастер-классов']);

const COLUMNS = [
    {
        Header: 'ID',
        accessor: 'id',
    },
    {
        Header: 'Дата создания',
        accessor: 'date',
        getProps: () => ({ type: 'date' }),
    },
    {
        Header: 'Название',
        accessor: 'name',
        getProps: () => ({ type: 'link' }),
    },
    {
        Header: 'Скидка',
        accessor: 'discount',
        getProps: () => ({ type: 'double' }),
    },
    {
        Header: 'Период действия',
        accessor: 'period',
        getProps: () => ({ type: 'dateRange' }),
    },
    {
        Header: 'Инициатор',
        accessor: 'initiator',
    },
    {
        Header: 'Статус',
        accessor: 'status',
    },
];

const Filters = ({
    className,
    initialValues,
    emptyInitialValues,
    onSubmit,
    onReset,
}: {
    className?: string;
    onSubmit: (vals: FormikValues) => void;
    onReset?: (vals: FormikValues) => void;
    emptyInitialValues: FormikValues;
    initialValues: FormikValues;
}) => {
    const [moreFilters, setMoreFilters] = useState(false);

    return (
        <Block className={className}>
            <DatepickerStyles />
            <Form initialValues={initialValues} onSubmit={onSubmit} onReset={onReset}>
                <Block.Body>
                    <Layout cols={12}>
                        <Layout.Item col={4}>
                            <Form.FastField name="ID" label="ID" />
                        </Layout.Item>
                        <Layout.Item col={4}>
                            <Form.FastField name="name" label="Название" />
                        </Layout.Item>
                        <Layout.Item col={4}>
                            <Form.FastField name="status" label="Статус">
                                <MultiSelect options={statuses} />
                            </Form.FastField>
                        </Layout.Item>
                        {moreFilters ? (
                            <>
                                <Layout.Item col={3}>
                                    <Form.FastField name="discounts" label="Скидка на">
                                        <MultiSelect options={discounts} />
                                    </Form.FastField>
                                </Layout.Item>
                                <Layout.Item col={3}>
                                    <Form.FastField name="role" label="Роль">
                                        <Select items={roles} placeholder="" />
                                    </Form.FastField>
                                </Layout.Item>
                                <Layout.Item col={3}>
                                    <Form.FastField name="initiator" label="Инициатор" />
                                </Layout.Item>
                                <Layout.Item col={3}>
                                    <Form.FastField name="author" label="Автор" />
                                </Layout.Item>
                                <Layout.Item col={6}>
                                    <Form.FastField label="Дата создания" name="creationDate">
                                        <DatepickerRange />
                                    </Form.FastField>
                                </Layout.Item>
                                <Layout.Item col={6}>
                                    <Form.FastField
                                        label="Период действия"
                                        name="perionDate"
                                        css={{ marginBottom: scale(2) }}
                                    >
                                        <DatepickerRange />
                                    </Form.FastField>

                                    <Form.FastField name="exactDiscountStartDate" css={{ marginBottom: scale(2) }}>
                                        <Switcher>
                                            <span>
                                                Точная дата начала скидки{' '}
                                                <Tooltip content="Искать точное совпадание с датой начала скидки">
                                                    <button type="button" css={{ verticalAlign: 'middle' }}>
                                                        <TipIcon />
                                                    </button>
                                                </Tooltip>
                                            </span>
                                        </Switcher>
                                    </Form.FastField>
                                    <Form.FastField name="exactDiscountEndDate">
                                        <Switcher>
                                            <span>
                                                Точная дата окончания скидки{' '}
                                                <Tooltip content="Искать точное совпадание с датой окончания скидки">
                                                    <button type="button" css={{ verticalAlign: 'middle' }}>
                                                        <TipIcon />
                                                    </button>
                                                </Tooltip>
                                            </span>
                                        </Switcher>
                                    </Form.FastField>
                                </Layout.Item>
                                <Layout.Item col="7/10">
                                    <Form.FastField name="indefiniteDate">
                                        <Switcher>Бессрочная скидка</Switcher>
                                    </Form.FastField>
                                </Layout.Item>
                            </>
                        ) : null}
                    </Layout>
                </Block.Body>
                <Block.Footer>
                    <div css={typography('bodySm')}>
                        Найдено 135 бандлов{' '}
                        <Button
                            theme="outline"
                            size="sm"
                            onClick={() => setMoreFilters(!moreFilters)}
                            css={{ marginLeft: scale(2) }}
                        >
                            {' '}
                            {moreFilters ? 'Меньше' : 'Больше'} фильтров
                        </Button>
                    </div>
                    <div>
                        <Form.Reset size="sm" theme="secondary" type="button" initialValues={emptyInitialValues}>
                            Сбросить
                        </Form.Reset>
                        <Button size="sm" theme="primary" css={{ marginLeft: scale(2) }} type="submit">
                            Применить
                        </Button>
                    </div>
                </Block.Footer>
            </Form>
        </Block>
    );
};

const newBundle = (id: number) => {
    return {
        id: `20${id}`,
        date: new Date(),
        name: getRandomItem([
            ['Резинка и стайер', `/marketing/discounts/20${id}`],
            ['Вентилятор и что-то еще', `/marketing/discounts/20${id}`],
        ]),
        discount: ['10% на', 'Бандл из товаров'],
        period: [new Date('2020-12-24'), new Date()],
        initiator: 'Маркетплейс',
        status: 'Активна',
    };
};
const makeBundles = (len: number) => [...Array(len).keys()].map(el => newBundle(el));

const MarketingBundles = () => {
    const data = useMemo(() => makeBundles(10), []);
    const [ids, setIds, selectedRows] = useSelectedRowsData<typeof data[0]>(data);

    const [isDeleteOpen, setIsDeleteOpen] = useState(false);
    const [isChangeOpen, setIsChangeOpen] = useState(false);

    const emptyInitialValues = {
        ID: '',
        name: '',
        status: '',
        discounts: '',
        role: '',
        initiator: '',
        creationDate: [null],
        perionDate: [null],
        exactDiscountStartDate: false,
        exactDiscountEndDate: false,
        indefiniteDate: false,
    };

    const { initialValues, URLHelper } = useURLHelper(emptyInitialValues);

    return (
        <PageWrapper h1="Бандлы">
            <>
                <Filters
                    initialValues={initialValues}
                    emptyInitialValues={emptyInitialValues}
                    onSubmit={vals => {
                        URLHelper(vals);
                    }}
                    css={{ marginBottom: scale(3) }}
                />

                <div css={{ display: 'flex', marginBottom: scale(3) }}>
                    <Button
                        size="sm"
                        theme="primary"
                        as={Link}
                        to="/marketing/bundles/create"
                        css={{ marginRight: scale(1) }}
                    >
                        Создать бандл
                    </Button>
                    {ids.length > 0 ? (
                        <>
                            <Button
                                size="sm"
                                theme="primary"
                                css={{ marginRight: scale(1) }}
                                onClick={() => setIsDeleteOpen(true)}
                                Icon={TrashIcon}
                            >
                                Удалить {`скидк${ids.length > 1 ? 'и' : 'у'}`}
                            </Button>
                            <Button
                                size="sm"
                                theme="primary"
                                css={{ marginRight: scale(1) }}
                                onClick={() => setIsChangeOpen(true)}
                                Icon={EditIcon}
                            >
                                Изменить статус {`скид${ids.length > 1 ? 'ок' : 'ки'}`}
                            </Button>
                        </>
                    ) : null}
                </div>

                <Block>
                    <Block.Body>
                        <Table
                            columns={COLUMNS}
                            data={data}
                            editRow={row => console.log('rowdata', row)}
                            onRowSelect={setIds}
                            needSettingsColumn={false}
                        />
                        {/* <Pagination url={pathname} activePage={activePage} pages={7} css={{ marginTop: scale(2) }} /> */}
                    </Block.Body>
                </Block>
                <Popup
                    isOpen={isDeleteOpen}
                    onRequestClose={() => setIsDeleteOpen(false)}
                    title="Вы уверены, что хотите удалить следующие скидки?"
                    popupCss={{ minWidth: scale(50) }}
                >
                    <ul css={{ marginBottom: scale(2) }}>
                        {selectedRows.map(r => (
                            <li key={r.id} css={{ marginBottom: scale(1, true) }}>
                                #{r.id} – {r.name[0]}
                            </li>
                        ))}
                    </ul>
                    <Button size="sm">Удалить</Button>
                </Popup>
                <Popup
                    isOpen={isChangeOpen}
                    onRequestClose={() => setIsChangeOpen(false)}
                    title="Обновление статуса"
                    popupCss={{ minWidth: scale(50) }}
                >
                    <ul css={{ marginBottom: scale(2) }}>
                        {selectedRows.map(r => (
                            <li key={r.id} css={{ marginBottom: scale(1, true) }}>
                                #{r.id} – {r.name[0]}
                            </li>
                        ))}
                    </ul>
                    <Form
                        initialValues={{ status: '' }}
                        onSubmit={() => {
                            console.log('send');
                        }}
                    >
                        <Form.Field name="status" label="Новый статус" css={{ marginBottom: scale(2) }}>
                            <Select items={statuses} placeholder="" />
                        </Form.Field>
                        <Button size="sm" type="submit">
                            Изменить статус
                        </Button>
                    </Form>
                </Popup>
            </>
        </PageWrapper>
    );
};

export default MarketingBundles;
