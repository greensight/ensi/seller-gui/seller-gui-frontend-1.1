import apiClient from '@api/index';

export interface Locations {
    results: {
        id: number;
        name: string;
        type: string;
    }[];
}

export const getLocations = async () => {
    const res: { data: Locations } = await apiClient('https://rickandmortyapi.com/api/location/');

    return res.data;
};
