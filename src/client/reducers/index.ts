import { combineReducers, Action } from 'redux';
import { ThunkAction } from 'redux-thunk';
import ua from './ua';
import profile from './profile';

const rootReducer = combineReducers({
    ua,
    profile,
});

export type RootState = ReturnType<typeof rootReducer>;
export type AppThunk<ReturnType = void> = ThunkAction<ReturnType, RootState, unknown, Action<string>>;
export default rootReducer;
