import React from 'react';
import { IMaskInput } from 'react-imask';
import { FieldHelperProps, FieldMetaProps, FieldInputProps } from 'formik';
import { CSSObject } from '@emotion/core';
import useFieldCSS from '@scripts/useFieldCSS';
export interface MaskProps {
    /** Mask for input */
    mask: string;
    /** Formik helpers object (inner) */
    helpers?: FieldHelperProps<string>;
    /** Placeholder for mask */
    placeholderChar?: string;
    /** Is show placholder */
    lazy?: boolean;
    /** from formik */
    field?: FieldInputProps<any>;
    /** from formik */
    meta?: FieldMetaProps<any>;
}

const Mask = ({ mask, meta, field, helpers, placeholderChar = '_', lazy = true, ...props }: MaskProps) => {
    const value = field?.value || undefined;
    const { basicFieldCSS } = useFieldCSS(meta);
    return (
        <IMaskInput
            mask={mask}
            value={value}
            {...props}
            css={basicFieldCSS}
            lazy={lazy}
            placeholderChar={placeholderChar}
            onAccept={(value: string) => {
                if (helpers) helpers.setValue(value);
            }}
        />
    );
};

export default Mask;
