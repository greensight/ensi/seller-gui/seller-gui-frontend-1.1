import * as React from 'react';
import Legend from '@standart/Legend';
import { scale, Layout } from '@greensight/gds';
import { FieldMetaProps, FieldInputProps, FieldHelperProps } from 'formik';
import Datepicker, { DatepickerProps, DatePickerType } from '@standart/Datepicker';

export type DatePickerRangeType = DatePickerType[] | null;
export interface DatepickerRangeProps extends Omit<DatepickerProps, 'field' | 'helpers' | 'selected'> {
    /** Name of datepicker range */
    name?: string;
    /** Formik field object */
    field?: FieldInputProps<DatePickerRangeType>;
    /** Datepicker range value */
    selected?: DatePickerRangeType;
    /** Formik helpers object */
    helpers?: FieldHelperProps<DatePickerRangeType>;
    /** Change handler */
    onChangeHandler?: (d: DatePickerRangeType) => void;
}

const DATEPICKER_QTY = 2;

export const DatepickerRange = ({
    name,
    field,
    selected,
    helpers,
    onChangeHandler,
    ...props
}: DatepickerRangeProps) => {
    const values = field?.value || selected || [null, null];

    const onChange = (date: Date | null, index: number) => {
        const temp = index ? [values[0], date] : [date, values[1]];
        if (helpers) helpers.setValue(temp);
        if (onChangeHandler) onChangeHandler(temp);
    };

    return (
        <Layout cols={2}>
            {[...new Array(DATEPICKER_QTY).keys()].map(index => {
                const [startDate, endDate] = values;
                return (
                    <Layout.Item col={1} key={`datepickerRange-${name}-${index}`}>
                        <Datepicker
                            selected={!index ? startDate : endDate}
                            startDate={startDate}
                            endDate={endDate}
                            maxDate={!index ? endDate : null}
                            minDate={index ? startDate : null}
                            placeholderText={!index ? 'от' : 'до'}
                            {...props}
                            onChange={(date: Date | null) => onChange(date, index)}
                        />
                    </Layout.Item>
                );
            })}
        </Layout>
    );
};

export default DatepickerRange;
