import React, { useMemo } from 'react';
import { useTheme, scale } from '@greensight/gds';
import Select, { OptionsType } from 'react-select';
import typography from '@scripts/typography';
import { CSSObject } from '@emotion/core';
import { FieldInputProps, FieldHelperProps } from 'formik';

/** Не удалось экспортировать prop types из пакета селекта. Поглядел исходник, видимо их там нет */
interface MultiSelectProps {
    name?: string;
    onChange?: (values: any) => void;
    placeholder?: string;
    isMulti?: boolean;
    options:
        | OptionsType<{ label: string; value: string }>
        | OptionsType<{ label: string; options: OptionsType<{ label: string; value: string }> }>;
    disabled?: boolean;
    field?: FieldInputProps<any>;
    helpers?: FieldHelperProps<any>;
    value?: string;
}

const MultiSelect = ({
    name,
    placeholder = '',
    disabled = false,
    onChange,
    isMulti = true,
    field,
    helpers,
    value,
    ...props
}: MultiSelectProps) => {
    const { colors } = useTheme();

    const customStyles = useMemo(
        () => ({
            option: (provided: CSSObject, state: { isSelected: boolean }) => ({
                ...provided,
                ...typography('bodySm'),
                minHeight: scale(3),
                cursor: 'pointer',
                display: 'flex',
                alignItems: 'center',
                backgroundColor: state.isSelected ? colors?.lightBlue : colors?.white,
                color: colors?.grey900,
                ':hover': { background: colors?.lightBlue },
            }),
            valueContainer: (provided: CSSObject) => ({
                ...provided,
                paddingTop: 0,
                paddingBottom: 0,
                ':hover': {
                    cursor: 'text',
                },
            }),
            control: () => ({
                display: 'flex',
                border: `1px solid ${colors?.grey400}`,
                borderRadius: 2,
                minHeight: scale(3),
                ...typography('bodySm'),
                ...(disabled
                    ? { backgroundColor: colors?.grey200, cursor: 'not-allowed', borderColor: colors?.grey600 }
                    : {}),
            }),
            clearIndicator: () => ({
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                cursor: 'pointer',
                width: scale(3),
                svg: { width: 14, height: 14 },
            }),
            indicatorSeparator: () => ({ display: 'none' }),
            dropdownIndicator: () => ({
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                width: scale(3),
                cursor: 'pointer',
                svg: { width: 16 },
            }),
            multiValueLabel: (provided: CSSObject) => ({
                ...provided,
                ...typography('small'),
                backgroundColor: colors?.grey200,
                height: scale(2),
                padding: 0,
            }),
            multiValueRemove: (provided: CSSObject) => ({
                ...provided,
                cursor: 'pointer',
                background: colors?.grey200,
                transition: 'transform 0.1s ease',
                ':hover': { background: colors?.grey300 },
            }),
            singleValue: (provided: CSSObject, state: { isDisabled: boolean }) => {
                const opacity = state.isDisabled ? 0.5 : 1;
                const transition = 'opacity 300ms';

                return { ...provided, opacity, transition };
            },
        }),
        [
            colors?.grey200,
            colors?.grey300,
            colors?.grey400,
            colors?.grey600,
            colors?.grey900,
            colors?.lightBlue,
            colors?.white,
            disabled,
        ]
    );

    const onValueChange = (values: any) => {
        if (onChange) onChange(values);
        if (helpers) helpers.setValue(values);
    };

    return (
        <Select
            styles={customStyles}
            onChange={onValueChange}
            placeholder={placeholder}
            isMulti={isMulti}
            value={value || field?.value}
            closeMenuOnSelect={!isMulti}
            {...props}
            id={name}
            isDisabled={disabled}
        />
    );
};

export default MultiSelect;
