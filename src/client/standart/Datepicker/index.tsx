import React from 'react';
import { FieldHelperProps, FieldMetaProps, FieldInputProps } from 'formik';
import DatePicker, { ReactDatePickerProps } from 'react-datepicker';
import { scale, useTheme } from '@greensight/gds';
import CalendarIcon from '@svg/calendar.svg';
import 'react-datepicker/dist/react-datepicker.css';

export type DatePickerType = Date | null;
export interface DatepickerProps extends Omit<ReactDatePickerProps, 'onChange' | 'selected'> {
    /** Selected date */
    selected?: Date | null;
    /** on Change Method */
    onChange?: (d: DatePickerType) => void;
    /** Formik field object (inner) */
    field?: FieldInputProps<DatePickerType>;
    /** Formik helpers */
    helpers?: FieldHelperProps<DatePickerType>;
    /** Formik meta */
    meta?: FieldMetaProps<DatePickerType>;
}

const Datepicker = ({ onChange, selected, field, helpers, meta, ...props }: DatepickerProps) => {
    const { colors } = useTheme();
    return (
        <div
            css={{
                width: '100%',
                position: 'relative',
                input: {
                    width: '100%',
                    border: `1px solid ${meta?.touched && meta?.error ? colors?.danger : colors?.grey400}`,
                    ':focus': { borderColor: colors?.primary },
                },
                '.react-datepicker-wrapper': {
                    width: '100%',
                },
            }}
        >
            <DatePicker
                locale="ru"
                dateFormat="dd.MM.yyyy"
                selected={field?.value || selected}
                showYearDropdown
                showMonthDropdown
                showPopperArrow={false}
                onChange={(date: DatePickerType) => {
                    if (onChange) onChange(date);
                    if (helpers) helpers.setValue(date);
                }}
                {...props}
            />

            <div
                css={{
                    position: 'absolute',
                    top: 0,
                    right: 0,
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    width: scale(6),
                    height: '100%',
                    pointerEvents: 'none',
                }}
            >
                <CalendarIcon />
            </div>
        </div>
    );
};

export default Datepicker;
