import React from 'react';
import { scale, useTheme, Container, Button } from '@greensight/gds';
import EnsiLogo from '@svg/tokens/logo/ensi.svg';
import typography from '@scripts/typography';
import LogoutIcon from '@svg/tokens/small/logOut.svg';

interface HeaderProps {
    /** on logout btn click method */
    onLogout: () => void;
}

const Header = ({ onLogout }: HeaderProps) => {
    const { colors, shadows } = useTheme();

    return (
        <header
            css={{
                backgroundColor: colors?.white,
                boxShadow: shadows?.big,
                height: scale(7),
                display: 'flex',
                alignItems: 'center',
                ...typography('bodySm'),
                justifyContent: 'space-between',
                paddingLeft: scale(3),
                paddingRight: scale(3),
            }}
        >
            <div css={{ display: 'flex', alignItems: 'center' }}>
                <EnsiLogo css={{ marginRight: scale(1) }} />
                Seller Administration system
            </div>
            <Button type="button" theme="ghost" size="sm" onClick={onLogout} Icon={LogoutIcon}>
                Выйти
            </Button>
        </header>
    );
};

export default Header;
