import React from 'react';
import { scale } from '@greensight/gds';
import { Helmet } from 'react-helmet-async';
import typography from '@scripts/typography';

const PageWrapper = ({ h1, title, children }: { h1?: string; title?: string; children: React.ReactNode }) => {
    return (
        <>
            <Helmet>
                <title>{title || h1}</title>
            </Helmet>
            <main css={{ flexGrow: 1, flexShrink: 1, padding: `${scale(2)}px ${scale(3)}px` }}>
                {h1 ? <h1 css={{ ...typography('h1'), marginBottom: scale(2), marginTop: 0 }}>{h1}</h1> : null}
                {children}
            </main>
        </>
    );
};

export default PageWrapper;
