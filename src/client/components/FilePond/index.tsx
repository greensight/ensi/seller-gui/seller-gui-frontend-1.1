import React, { useState } from 'react';
import { FilePond as ReactFilePond, FilePondProps as ReactFilePondProps, registerPlugin } from 'react-filepond';
import 'filepond/dist/filepond.min.css';
import 'filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css';
import FilePondPluginImagePreview, { FilePondPluginImagePreviewProps } from 'filepond-plugin-image-preview';
import FilePondPluginFileValidateType, {
    FilePondPluginFileValidateTypeProps,
} from 'filepond-plugin-file-validate-type';
import FilePondPluginFileValidateSize, {
    FilePondPluginFileValidateSizeProps,
} from 'filepond-plugin-file-validate-size';

registerPlugin(FilePondPluginImagePreview, FilePondPluginFileValidateType, FilePondPluginFileValidateSize);

interface ComponentProps {
    imageLayout?: boolean;
    maxFileSize?: string;
    maxTotalFileSize?: string;
}

type FilePondProps = ComponentProps &
    ReactFilePondProps &
    FilePondPluginFileValidateTypeProps &
    FilePondPluginImagePreviewProps &
    Omit<FilePondPluginFileValidateSizeProps, 'maxFileSize' | 'maxTotalFileSize'>;

const FilePond = ({
    imageLayout,
    maxFileSize = '10MB',
    maxTotalFileSize = '100MB',
    allowMultiple = true,
    maxFiles = 10,
    ...props
}: FilePondProps) => {
    const [files, setFiles] = useState<File[]>([]);

    return (
        <div className={`file-input${imageLayout ? ' image-preview' : ''}`}>
            <ReactFilePond
                files={files}
                onupdatefiles={fileItems => {
                    setFiles(fileItems.map(f => f.file));
                }}
                // @ts-ignore
                maxFileSize={maxFileSize}
                maxTotalFileSize={maxTotalFileSize}
                maxFiles={maxFiles}
                credits={false}
                allowMultiple={allowMultiple}
                allowReorder={true}
                labelInvalidField="Неверный формат файлов"
                labelFileWaitingForSize="Ожидание размера файла"
                labelFileSizeNotAvailable="Размер файла недоступен"
                labelFileLoading="Загружается"
                labelFileLoadError="Ошибка в процессе загрузки"
                labelFileProcessing="Загружается"
                labelFileProcessingComplete="Загрузка завершена"
                labelFileProcessingAborted="Загрузка отменена"
                labelFileProcessingError="Ошибка в процессе загрузки"
                labelFileProcessingRevertError="Ошибка в процессе отмены"
                labelFileRemoveError="Ошибка в процессе удаления"
                labelTapToCancel="Нажмите чтобы отменить"
                labelTapToRetry="Нажмите чтобы повторить"
                labelTapToUndo="Нажмите чтобы отменить"
                labelButtonRemoveItem="Удалить"
                labelButtonAbortItemLoad="Отменить"
                labelButtonRetryItemLoad="Повторить"
                labelButtonAbortItemProcessing="Отмена"
                labelButtonUndoItemProcessing="Отменить"
                labelButtonRetryItemProcessing="Повторить"
                labelButtonProcessItem="Загрузить"
                labelMaxFileSizeExceeded="Файл слишком большой"
                labelMaxFileSize="Максимальный размер файла {filesize}"
                labelFileTypeNotAllowed="Неверный тип файла"
                fileValidateTypeLabelExpectedTypes="Загрузите {allButLastType} {lastType}"
                name="files"
                labelIdle={`<span class="filepond--label-action">Выберите файл${
                    allowMultiple && maxFiles !== 1 ? 'ы' : ''
                }</span> или перетащите ${allowMultiple && maxFiles !== 1 ? 'их' : 'его'} сюда`}
                {...props}
            />
        </div>
    );
};

export default FilePond;
