import React, { useEffect, useMemo, useState } from 'react';
import { scale, useTheme, Button } from '@greensight/gds';
import {
    useTable,
    useSortBy,
    useRowSelect,
    Row,
    Cell as CellProps,
    ColumnInstance,
    HeaderGroup,
    TableHeaderProps,
    UseTableInstanceProps,
    TableState,
} from 'react-table';
import { DragDropContext, Droppable, Draggable, DropResult } from 'react-beautiful-dnd';
import { CSSObject } from '@emotion/core';
import loadable from '@loadable/component';

import typography from '@scripts/typography';
import usePrevious from '@scripts/usePrevious';

import ArrowDownIcon from '@svg/tokens/small/arrowDown.svg';
import ArrowUpIcon from '@svg/tokens/small/arrowUp.svg';
import ClosedIcon from '@svg/tokens/medium/closed.svg';
import TrashIcon from '@svg/tokens/small/trash.svg';
import SettingsIcon from '@svg/tokens/small/settings.svg';
import EditIcon from '@svg/tokens/small/edit.svg';

import IndeterminateCheckbox from './IndeterminateCheckbox';
import Cell from './Cell';

const ColumnsSettingsPopup = loadable(() => import('./ColumnsSettingsPopup'));

export type TableColumnProps = { Header: string | any };
export type TableRowProps = Record<string, any>;

export interface MoreHeaderGroupProps extends HeaderGroup<TableRowProps> {
    isSorted?: boolean;
    isSortedDesc?: boolean;
    getSortByToggleProps?: () => Partial<TableHeaderProps>;
}

export interface MoreRow extends Row<TableRowProps> {
    isSelected?: boolean;
}

export interface MoreColumnProps extends ColumnInstance<TableRowProps> {
    getProps?: () => {
        type: string;
    };
    customRender?: boolean;
}

export interface MoreCellProps extends CellProps<TableRowProps, any> {
    column: MoreColumnProps;
}

interface extendedTableState extends TableState<TableRowProps> {
    selectedRowIds: Record<string, boolean>;
}
interface extendedUseTableInstanceProps extends UseTableInstanceProps<TableRowProps> {
    state: extendedTableState;
}

export interface TableProps {
    columns: TableColumnProps[];
    data: TableRowProps[];
    /** handle row info */
    editRow?: (originalRow: TableRowProps | undefined) => void;
    /** delete row */
    deleteRow?: (originalRow: TableRowProps | undefined) => void;
    /** On row select handler */
    onRowSelect?: (ids: number[]) => void;
    /** Need checkboxes column */
    needCheckboxesCol?: boolean;
    /** Need settings column */
    needSettingsColumn?: boolean;
    /** Class name */
    className?: string;
    /** Breadcrumbs item content */
    children?: React.ReactElement;
    /** isDragDisabled */
    isDragDisabled?: boolean;
    /** if you need change incoming data */
    setData?: (data: any) => void;
    /** on settings click */
    onSettingsClick?: () => void;
}

export const makeStringRow = (id: string, string: string) => ({ id, string });
export const makeStringDeleteRow = (id: string, string: string, onDelete: (id: string) => void) => ({
    id,
    string,
    onDelete,
});

const Table = ({
    columns,
    data,
    editRow,
    deleteRow,
    onRowSelect,
    needCheckboxesCol = true,
    needSettingsColumn = true,
    className,
    children,
    isDragDisabled = true,
    setData,
}: TableProps) => {
    const [isOpen, setIsOpen] = useState(false);
    const { colors } = useTheme();
    const arrowStyle: CSSObject = {
        fill: colors?.primary,
        marginLeft: scale(1),
        marginBottom: -scale(1, true),
    };

    const tableInstanse = useTable(
        {
            columns,
            data,
        },
        useSortBy,
        useRowSelect,
        hooks => {
            if (needCheckboxesCol) {
                hooks.visibleColumns.push(columns => [
                    {
                        id: 'selection',
                        Header: ({ getToggleAllRowsSelectedProps }: { getToggleAllRowsSelectedProps: () => void }) => (
                            <IndeterminateCheckbox {...getToggleAllRowsSelectedProps()} id="header" />
                        ),
                        Cell: ({
                            row,
                        }: {
                            row: { getToggleRowSelectedProps: () => void; original: { id: string } };
                        }) => <IndeterminateCheckbox {...row.getToggleRowSelectedProps()} id={row.original.id} />,
                    },
                    ...columns,
                ]);
            }
        }
    );

    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        rows,
        prepareRow,
        allColumns,
        getToggleHideAllColumnsProps,
        state: { selectedRowIds },
    } = (tableInstanse as any) as extendedUseTableInstanceProps;

    const selectedRowIdsArray = useMemo(() => {
        return selectedRowIds ? Object.keys(selectedRowIds).map(Number) : [];
    }, [selectedRowIds]);
    const prevSelectedRow = usePrevious(selectedRowIdsArray);
    useEffect(() => {
        if (JSON.stringify(prevSelectedRow) !== JSON.stringify(selectedRowIdsArray)) {
            if (onRowSelect) onRowSelect(selectedRowIdsArray);
        }
    }, [onRowSelect, prevSelectedRow, selectedRowIdsArray]);

    const reorderTable = React.useCallback(
        (startIndex: number, endIndex: number) => {
            const newData = [...data];
            const [movedRow] = newData.splice(startIndex, 1);
            newData.splice(endIndex, 0, movedRow);
            if (setData) setData(newData);
        },
        [data, setData]
    );

    const onDragEnd = React.useCallback(
        ({ source, destination }: DropResult) => {
            if (!destination || destination.index === source.index) return;
            reorderTable(source.index, destination.index);
        },
        [reorderTable]
    );
    const childrenWidth: string[] = children?.props?.children?.map((c: React.ReactElement) => c.props.width);

    return (
        <div css={{ width: '100%', overflow: 'auto' }} className={className}>
            <table
                {...getTableProps()}
                css={{
                    width: '100%',
                    borderSpacing: 0,
                    borderCollapse: 'collapse',
                    position: 'relative',
                }}
            >
                {children}

                <thead>
                    {headerGroups.map((headerGroup, key) => (
                        <tr {...headerGroup.getHeaderGroupProps()} key={key}>
                            {headerGroup.headers.map(
                                (column: MoreHeaderGroupProps) =>
                                    column?.getSortByToggleProps && (
                                        <th
                                            {...column.getHeaderProps(column?.getSortByToggleProps())}
                                            css={{
                                                ...typography('smallBold'),
                                                padding: scale(1),
                                                textAlign: 'left',
                                                borderBottom: `1px solid ${colors?.grey400}`,
                                                whiteSpace: 'nowrap',
                                                verticalAlign: 'top',
                                            }}
                                            key={column.id}
                                        >
                                            {column.render('Header')}
                                            {column.isSorted && column.isSortedDesc && (
                                                <ArrowDownIcon css={arrowStyle} />
                                            )}
                                            {column.isSorted && !column.isSortedDesc && (
                                                <ArrowUpIcon css={arrowStyle} />
                                            )}
                                        </th>
                                    )
                            )}
                            {needSettingsColumn && (
                                <th
                                    css={{
                                        ...typography('smallBold'),
                                        paddingBottom: scale(1),
                                        textAlign: 'left',
                                        borderBottom: `1px solid ${colors?.grey400}`,
                                        whiteSpace: 'nowrap',
                                    }}
                                    key="new"
                                >
                                    <Button
                                        type="button"
                                        aria-label="Удалить"
                                        size="sm"
                                        Icon={SettingsIcon}
                                        hidden
                                        theme="ghost"
                                        onClick={() => setIsOpen(true)}
                                    >
                                        Settings
                                    </Button>
                                </th>
                            )}
                        </tr>
                    ))}
                </thead>
                <DragDropContext onDragEnd={onDragEnd}>
                    <Droppable droppableId="droppable-table-id">
                        {provided => (
                            <tbody {...getTableBodyProps()} ref={provided.innerRef} {...provided.droppableProps}>
                                {rows.map((row: MoreRow, i) => {
                                    prepareRow(row);
                                    const textRow = Object.keys(row.original).length === 2 && row.original?.string;
                                    const textDeleteRow =
                                        Object.keys(row.original).length === 3 &&
                                        row.original?.onDelete &&
                                        row.original?.string;
                                    return (
                                        <Draggable
                                            draggableId={row.original.id.toString()}
                                            key={row.original.id}
                                            index={i}
                                            isDragDisabled={isDragDisabled}
                                        >
                                            {(draggableProvided, snapshot) => (
                                                <tr
                                                    {...row.getRowProps()}
                                                    key={i}
                                                    css={{
                                                        borderBottom: `1px solid ${colors?.grey400}`,
                                                        '&:last-child': {
                                                            border: 'none',
                                                        },
                                                        backgroundColor: colors?.white,
                                                        ...(snapshot.isDragging ? { display: 'flex' } : {}),
                                                    }}
                                                    ref={draggableProvided.innerRef}
                                                    {...draggableProvided.dragHandleProps}
                                                    {...draggableProvided.draggableProps}
                                                >
                                                    {textRow ? (
                                                        <td
                                                            css={{
                                                                verticalAlign: 'top',
                                                                padding: scale(1),
                                                                ...typography('bodySm'),
                                                                ...(childrenWidth ? { width: childrenWidth[0] } : {}),
                                                            }}
                                                            colSpan={row.allCells.length + Number(needSettingsColumn)}
                                                            key={`1-${row.id}`}
                                                        >
                                                            <p>
                                                                <strong>{row.original.string}</strong>
                                                            </p>
                                                        </td>
                                                    ) : textDeleteRow ? (
                                                        <td
                                                            css={{
                                                                verticalAlign: 'top',
                                                                padding: scale(1),
                                                                ...typography('bodySm'),
                                                                ...(childrenWidth ? { width: childrenWidth[0] } : {}),
                                                            }}
                                                            colSpan={row.allCells.length + Number(needSettingsColumn)}
                                                            key={`1-${row.id}`}
                                                        >
                                                            <p
                                                                css={{
                                                                    display: 'flex',
                                                                    justifyContent: 'space-between',
                                                                }}
                                                            >
                                                                <strong>{row.original.string}</strong>
                                                                <ClosedIcon
                                                                    onClick={() => {
                                                                        row.original.onDelete(row.original.id);
                                                                    }}
                                                                    css={{ fill: colors?.grey800 }}
                                                                />
                                                            </p>
                                                        </td>
                                                    ) : (
                                                        row.cells.map((cell: MoreCellProps, index) => {
                                                            const props =
                                                                cell.column?.getProps && cell.column.getProps();
                                                            const type = props && props.type;

                                                            return (
                                                                <td
                                                                    css={{
                                                                        verticalAlign: 'top',
                                                                        padding: scale(1),
                                                                        ...typography('bodySm'),
                                                                        ...(childrenWidth
                                                                            ? { width: childrenWidth[index] }
                                                                            : {}),
                                                                    }}
                                                                    {...cell.getCellProps()}
                                                                    key={`${cell.column.id}-${cell.row.id}`}
                                                                >
                                                                    {cell.column.id === 'selection' ||
                                                                    cell.column.customRender ? (
                                                                        cell.render('Cell')
                                                                    ) : (
                                                                        <Cell text={cell?.value} type={type} />
                                                                    )}
                                                                </td>
                                                            );
                                                        })
                                                    )}
                                                    {needSettingsColumn && !textRow && !textDeleteRow && (
                                                        <td
                                                            css={{
                                                                padding: scale(1),
                                                                verticalAlign: 'top',
                                                                ...(childrenWidth
                                                                    ? { width: childrenWidth[childrenWidth.length - 1] }
                                                                    : {}),
                                                            }}
                                                        >
                                                            {editRow && (
                                                                <Button
                                                                    onClick={() => editRow(row?.original)}
                                                                    aria-label="Редактировать"
                                                                    size="sm"
                                                                    Icon={EditIcon}
                                                                    hidden
                                                                    theme="ghost"
                                                                >
                                                                    Редактировать
                                                                </Button>
                                                            )}
                                                            {deleteRow && (
                                                                <Button
                                                                    onClick={() => deleteRow(row?.original)}
                                                                    aria-label="Удалить"
                                                                    size="sm"
                                                                    Icon={TrashIcon}
                                                                    hidden
                                                                    theme="ghost"
                                                                >
                                                                    Удалить
                                                                </Button>
                                                            )}
                                                        </td>
                                                    )}
                                                </tr>
                                            )}
                                        </Draggable>
                                    );
                                })}
                                {provided.placeholder}
                            </tbody>
                        )}
                    </Droppable>
                </DragDropContext>
            </table>
            <ColumnsSettingsPopup
                isOpen={isOpen}
                close={() => setIsOpen(false)}
                getToggleHideAllColumnsProps={getToggleHideAllColumnsProps}
                allColumns={allColumns}
            />
        </div>
    );
};

export default Table;
