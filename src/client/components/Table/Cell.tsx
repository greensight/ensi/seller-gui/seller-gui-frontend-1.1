import React from 'react';
import { format } from 'date-fns';
import { Link, useLocation } from 'react-router-dom';
import Badge from '@components/Badge';
import Picture from '@standart/Picture';
import { convertPrice } from '@scripts/helpers';
import noImage from '@images/simple/noimage.png';
import { CELL_TYPES } from '@scripts/enums';
import { useTheme, scale } from '@greensight/gds';
import getStatusType from '@scripts/getStatusType';
import Export from '@svg/export.svg';

interface CellProps {
    text?: string;
    type?: string;
}

const Cell = ({ text, type }: CellProps) => {
    const { colors } = useTheme();
    const { pathname } = useLocation();

    if (type === CELL_TYPES.PHOTO) {
        return <Picture css={{ width: scale(6), height: scale(6), maxWidth: 'none' }} image={text || noImage} alt="" />;
    }

    if (typeof text !== 'number' && !text) return <p>-</p>;

    if (!type) return <p>{text}</p>;

    if (type === CELL_TYPES.LINKED_ID) {
        return (
            <Link to={`${pathname}/${text}`} css={{ color: colors?.primary }}>
                {text}
            </Link>
        );
    }

    if (type === CELL_TYPES.LINK) {
        const [name, to] = text;
        return (
            <Link to={to} css={{ color: colors?.primary }}>
                {name}
            </Link>
        );
    }

    if (type === CELL_TYPES.DOUBLE) {
        const [title, descr] = text;

        return (
            <div>
                <p>{title}</p> <p css={{ color: colors?.grey800 }}>{descr}</p>
            </div>
        );
    }

    if (type === CELL_TYPES.STATUS) return <Badge text={text} type={getStatusType(text)} />;

    if (type === CELL_TYPES.PRICE) {
        const [rub, penny] = text.toString().split('.');
        return <span css={{ whiteSpace: 'nowrap' }}>{convertPrice(rub, penny)}</span>;
    }

    if (type === CELL_TYPES.DATE) return <p>{format(new Date(text), 'dd.MM.yyyy')}</p>;

    if (type === CELL_TYPES.DOCUMENT_DOWNLOAD)
        return (
            <Link to={`${pathname}/${text}`} target="_blank">
                <Export css={{ fill: colors?.grey800 }} />
            </Link>
        );

    if (type === CELL_TYPES.DATE_RANGE) {
        const [from, to] = text;
        return (
            <p>
                c {format(new Date(from), 'dd.MM.yyyy')} по {format(new Date(to), 'dd.MM.yyyy')}
            </p>
        );
    }

    if (type === CELL_TYPES.ARRAY)
        return (
            <>
                {Array.isArray(text) &&
                    text.map((item, index) => (
                        <p css={{ marginTop: index > 0 ? scale(1) : 0 }} key={index}>
                            {item}
                        </p>
                    ))}
            </>
        );

    return <p>-</p>;
};

export default Cell;
