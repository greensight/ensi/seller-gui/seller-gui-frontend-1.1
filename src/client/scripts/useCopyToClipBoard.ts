import { useCallback } from 'react';

const useCopyToClipBoard = (data: any[]) => {
    return useCallback(() => {
        const selectedIDs = data.map((d: { id: string }) => d.id);
        navigator.clipboard.writeText(selectedIDs.join(',')).catch(() => alert('Не удалось скопировать ID!'));
    }, [data]);
};

export { useCopyToClipBoard };
