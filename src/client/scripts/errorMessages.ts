const errorMessages = {
    required: 'Обязательное поле',
    email: 'Неверный формат email',
};

export { errorMessages };
