export type MergeElementProps<T extends React.ElementType, P extends unknown> = Omit<
    React.ComponentPropsWithRef<T>,
    keyof P
> &
    P;
