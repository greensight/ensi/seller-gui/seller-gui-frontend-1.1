import { STATUSES, DELIVERY_SERVICE_STATUSES, CHECK_REQUEST_STATUSES, STOCKS } from '@scripts/data/different';
import { nanoid } from 'nanoid';

const mock = async <T>({
    time = 500,
    data,
}: {
    time?: number;
    data?: T;
} = {}) => {
    await new Promise(resolve => setTimeout(resolve, time));
    return { data };
};

export default mock;

export interface Categories {
    id: number;
    name: string;
    subCategories?: Categories[];
}

export const categoriesData: Categories[] = [
    {
        id: 1,
        name: 'Мебель',
        subCategories: [{ id: 30, name: 'Тестовая категория' }],
    },
    {
        id: 2,
        name: 'Ножницы',
    },
    {
        id: 3,
        name: 'Резинки для волос',
    },
    {
        id: 4,
        name: 'Стайлеры',
    },
    {
        id: 5,
        name: 'Тест',
    },
    {
        id: 6,
        name: 'Товары для дома',
        subCategories: [{ id: 7, name: 'Текстиль для дома' }],
    },
    {
        id: 23,
        name: 'Шампуни',
        subCategories: [
            { id: 8, name: 'Восстанавливающие шампуни' },
            {
                id: 12,
                name: 'Профессиональные шампуни',
                subCategories: [{ id: 10, name: 'Шампуни без парабентов' }],
            },
        ],
    },
];

export const mockSearchResultItems = [
    {
        name: 'Крупа ячменная',
        image: 'https://placehold.it/48x48',
        price: '199',
        link: '/product/krupa-yachmennaya',
    },
    {
        category: 'Клиенты',
        name: 'Клиентская база',
        link: '/clients',
    },
    {
        name: 'Крупа пшеничная',
        image: 'https://placehold.it/48x48',
        price: '55.2',
        link: '/product/krupa-yachmennaya',
    },
    {
        category: 'Справочники',
        name: 'Категории',
        link: '/spravochnik',
    },
    {
        name: 'Андрей Сергеевич Попов',
        category: 'Контакты',
        link: '/product/krupa-yachmennaya',
    },
    {
        name: 'Василий Петрович Попов',
        category: 'Контакты',
        link: '/contacts',
    },
    {
        name: 'Иван Иванович Иванов',
        category: 'Контакты',
        link: '/contacts',
    },
    {
        category: 'Клиенты',
        name: 'Клиентская база',
        link: '/clients',
    },
    {
        name: 'Рис цельнозерненый',
        image: 'https://placehold.it/48x48',
        price: '88.34',
        link: '/product/krupa-yachmennaya',
    },
    {
        category: 'Клиенты',
        name: 'Клиентская база',
        link: '/clients',
    },
];

export const columns = [
    {
        Header: 'ID',
        accessor: 'id',
    },
    {
        Header: '',
        accessor: 'photo',
        getProps: () => ({ type: 'photo' }),
    },
    {
        Header: 'Название, артикул',
        accessor: 'titleAndCode',
        getProps: () => ({ type: 'double' }),
    },
    {
        Header: 'Бренд',
        accessor: 'brand',
    },
    {
        Header: 'Категория',
        accessor: 'category',
    },
    {
        Header: 'Создано',
        accessor: 'created',
        getProps: () => ({ type: 'date' }),
    },
    {
        Header: 'Цена, ₽',
        accessor: 'price',
        getProps: () => ({ type: 'price' }),
    },
    {
        Header: 'Кол-во',
        accessor: 'count',
    },
    {
        Header: 'Статус',
        accessor: 'status',
        getProps: () => ({ type: 'status' }),
    },
    {
        Header: 'В архиве',
        accessor: 'archive',
        getProps: () => ({ type: 'status' }),
    },
];

export const sellers = ['ООО "Рога и копыта"', 'Ашан', 'Леруа Мерлен', 'Б.Ю. Александров', 'М.П. Почтомат'];
export const deliveryServices = ['B2Cpl', 'Boxberry', 'СДЭК'];
export const warehouses = ['Силино', 'Алабушево', 'Лунево', 'Андреевка'];
export const cargoStatuses = [
    'Создан',
    'Передан Логистическому Оператору',
    'Принят Логистическим Оператором',
    'Отменён',
];

export const getRandomItem = (arr: any[]) => arr[Math.floor(Math.random() * arr.length)];
const newTableItem = (id: number) => {
    return {
        id: `2003${id}`,
        photo: getRandomItem([
            'https://spoonacular.com/cdn/ingredients_100x100/apple.jpg',
            '',
            'https://spoonacular.com/cdn/ingredients_100x100/orange.jpg',
        ]),
        titleAndCode: getRandomItem([
            ['Бургер из свинины и говядины «ПРОМАГРО», охлажденный, 200 г', 4650096570695],
            ['Бургер из свинины и говядины', 4650096570695],
        ]),
        brand: getRandomItem(['ПРОМАГРО', 'Мираторг']),
        category: 'Полуфабрикаты мясные, фарш',
        created: new Date(),
        price: 199838,
        count: getRandomItem([15, 32, 64, 71, 86, 43, 25, 13]),
        status: 'Согласовано',
        archive: getRandomItem(['Да', 'Нет']),
    };
};

const requestersTableItem = (id: number) => {
    return {
        id: `2003${id}`,
        author: getRandomItem(['Резинкин Петр Петрович', 'Петров Петр Петрович', 'Иванов Иван Иванович']),
        status: getRandomItem(CHECK_REQUEST_STATUSES),
        quantity: getRandomItem([15, 32, 64, 71, 86, 43, 25, 13]),
        created: new Date(),
    };
};

const offersTableItem = (id: number) => {
    return {
        id: `100${id}`,
        title: getRandomItem([
            'Резинка для волос женская и детская, прочная и долговечная (браслет пружинка), набор из 3 шт.',
            'Стайлер для волос IKOO E-styler Pro White Platina',
            'Декоративная игрушка',
            'Подушка',
            'Карандаш',
            'Бутылка',
            'Мячик',
            'Шарик',
            'Ноутбук',
        ]),
        seller: getRandomItem(sellers),
        status: getRandomItem(STATUSES),
        price: getRandomItem([15, 32, 64, 71, 86, 43, 25, 13]),
        residue: getRandomItem([100, 25, 13, 512, 2000, 2000]),
        created: new Date(),
    };
};

const deliveryServicesTableItem = (id: number) => {
    return {
        id: `${id}`,
        title: getRandomItem([
            'Boxberry',
            'Dostavista',
            'DPD',
            'IML',
            'MaxiPost',
            'PickPoint',
            'PONY EXPRESS',
            'B2Cpl',
            'Почта России',
            'СДЭК',
        ]),
        status: getRandomItem(DELIVERY_SERVICE_STATUSES),
        priority: getRandomItem([1, 2, 3]),
    };
};

const productTableItem = (id: number) => {
    return {
        id: `100${id}`,
        photo: getRandomItem(['https://picsum.photos/300/300', '']),
        title: getRandomItem([
            ['Резинка для волос женская и детская, прочная и долговечная (браслет пружинка), набор из 3 шт.', 25435],
            ['Стайлер для волос IKOO E-styler Pro White Platina', 12344],
            ['Декоративная игрушка', 112233],
            ['Подушка', 11223344],
            ['Карандаш', 12344321],
            ['Бутылка', 43214321],
            ['Мячик', 12344321],
            ['Шарик', 1234],
            ['Ноутбук', 4311234],
        ]),
        brand: getRandomItem(['letual', 'brand', 'addidas']),
        category: getRandomItem(['cat 1', 'cat 2', 'cat 3']),
        price: getRandomItem([15, 32, 64, 71, 86, 43, 25, 13]),
        quantity: getRandomItem([100, 25, 13, 512, 2000, 2000]),
        date: getRandomItem([new Date(), new Date('2021/02/23')]),
        active: getRandomItem(['да', 'нет', '---']),
        agreed: getRandomItem(['согласовано', 'не согласовано', 'в процессе']),
        archive: getRandomItem(['да', 'нет', '---']),
        content: getRandomItem(['cъмка одобрена', 'на согласовнии', 'на съемках']),
        shoppilot: getRandomItem(['да', 'нет', 'нет информации']),
    };
};

const shipmentsTableItem = (id: number) => {
    return {
        id: [`100${id}`, `/shipment/list/100${id}`],
        dateOrder: getRandomItem([new Date(), new Date('2021/02/23')]),
        sum: getRandomItem([1500, 1000, 3800, 5120, 2000, 5000]),
        dateShipment: getRandomItem([new Date(), new Date('2021/02/23')]),
        stock: getRandomItem(STOCKS),
        status: getRandomItem(STATUSES),
        countBox: getRandomItem([1, 2, 3, 4]),
    };
};

const pricesTableItems = (id: number) => {
    return {
        id: `100${id}`,
        author: getRandomItem([
            ['Чирва М.В.', 'test@test.ru'],
            ['Чирва М.В.', 'test@test.ru'],
            ['Чирва М.В.', 'test@test.ru'],
        ]),
        status: getRandomItem(STATUSES),
        quantity: getRandomItem([100, 25, 13, 512, 2000, 2000]),
        created: new Date(),
    };
};

const claimContentItem = (id: number) => {
    return {
        id: `100${id}`,
        type: getRandomItem(['Тестовое описание', 'Фото/видео съемка', 'Съёмка и текст']),
        status: getRandomItem([
            'Новая',
            'В обработке',
            'В продакшен',
            'Загрузка контента',
            'Возврат на склад',
            'Завершено',
            'Отклонена',
        ]),
        quantity: getRandomItem([100, 25, 13, 512, 2000, 2000]),
        shootingType: getRandomItem(['Без вскрытия', 'Со вскрытием']),
        claimAuthor: getRandomItem([
            ['Оболенский И. Я.', 'obolensky@mail.ru'],
            ['Чирва М. В.', 'chirva@mail.ru'],
        ]),
        created: new Date(),
        document: getRandomItem([
            'claims/content/15/documents/acceptance-act',
            'claims/content/16/documents/acceptance-act',
            '',
            null,
            undefined,
        ]),
    };
};
export const importTypes = ['Импорт товаров', 'Импорт цен', 'Импорт остатков'];
export const importStatuses = ['Создан', 'В обработке', 'Выполнен', 'Не выполнен'];

const importsTableItem = (id: number) => {
    return {
        id: `100${id}`,
        types: getRandomItem(importTypes),
        created: new Date(),
        statuses: getRandomItem(importStatuses),
        start: new Date(),
        ending: new Date(),
    };
};

const cargosTableItem = (id: number) => {
    return {
        id: `100${id}`,
        created: new Date(),
        delivery: getRandomItem(deliveryServices),
        warehouse: getRandomItem(warehouses),
        status: getRandomItem(cargoStatuses),
        boxes: getRandomItem([15, 32, 64, 71, 86, 43, 25, 13]),
        quantity: Math.floor(Math.random() * 10) + 1,
        comment: getRandomItem(['Комментарий', '']),
        price: Math.floor(Math.random() * 10000) + 1.1,
        discount: Math.floor(Math.random() * 100) + 1.05,
        weigth: Math.floor(Math.random() * 1000) + 1,
        pickupNumber: Math.floor(Math.random() * 1000) + 1,
    };
};

export function makeRandomData<T>(len: number, cb: (index: number) => T) {
    return [...Array(len).keys()].map(el => cb(el));
}

export const makeOffers = (len: number) => [...Array(len).keys()].map(el => offersTableItem(el));
export const makeImports = (len: number) => [...Array(len).keys()].map(el => importsTableItem(el));
export const makeProducts = (len: number) => [...Array(len).keys()].map(el => productTableItem(el));
export const makeShipments = (len: number) => [...Array(len).keys()].map(el => shipmentsTableItem(el));

export const makeData = (len: number) => [...Array(len).keys()].map(el => newTableItem(el));
export const makeDeliveryServices = (len: number) => [...Array(len).keys()].map(el => deliveryServicesTableItem(el));
export const makeRequesters = (len: number) => [...Array(len).keys()].map(el => requestersTableItem(el));
export const makePricesChanges = (len: number) => [...Array(len).keys()].map(el => pricesTableItems(el));
export const makeClaimsContent = (len: number) => [...Array(len).keys()].map(el => claimContentItem(el));

export const makeCargos = (len: number) => makeRandomData<ReturnType<typeof cargosTableItem>>(len, cargosTableItem);

export const shipmentData = {
    id: `1000`,
    created: new Date(),
    warehouse: 'Силино',
    statusText: 'Передано логистическому оператору',
    boxes: 71,
    quantity: 8,
    price: 8641.1,
    discount: 74.05,
    weigth: 715,
};

export const shipmentOrderList = (id: number) => {
    return {
        id: nanoid(5),
        photo: getRandomItem(['https://picsum.photos/300/300', '']),
        title: getRandomItem([
            ['Резинка для волос женская и детская, прочная и долговечная (браслет пружинка), набор из 3 шт.', 25435],
            ['Стайлер для волос IKOO E-styler Pro White Platina', 12344],
            ['Декоративная игрушка', 112233],
            ['Подушка', 11223344],
            ['Карандаш', 12344321],
            ['Бутылка', 43214321],
            ['Мячик', 12344321],
            ['Шарик', 1234],
            ['Ноутбук', 4311234],
        ]),
        category: getRandomItem([
            ['Канцтовары', 'Pilot'],
            ['Одежда', 'Adidas'],
            ['Игрушки', 'Lego'],
        ]),
        price: Math.floor(Math.random() * 10000),
        discount: Math.floor(Math.random() * 10000),
        priceWithDiscount: Math.floor(Math.random() * 10000),
        sum: Math.floor(Math.random() * 10000),
        sumDiscount: Math.floor(Math.random() * 10000),
        sumWithDiscount: Math.floor(Math.random() * 10000),
    };
};

export const makeShipmentOrderList = (len: number) => [...Array(len).keys()].map(el => shipmentOrderList(el));

export const shipmentHistory = (id: number) => {
    return {
        id: `100${id}`,
        essense: getRandomItem(['(ID: 26)', '(ID: 115)', '(ID: 32)', '(ID: 130)']),
        date: new Date(),
        action: getRandomItem(['Добавление связи одной сущности к другой', 'Обновление сущности', 'Создание сущности']),
        user: getRandomItem(['Система', 'super-admin@greensight.ru (ID: 1)']),
    };
};

export const makeShipmentHistory = (len: number) => [...Array(len).keys()].map(el => shipmentHistory(el));
