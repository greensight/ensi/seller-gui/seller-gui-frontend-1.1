export enum CELL_TYPES {
    PRICE = 'price',
    STATUS = 'status',
    DOUBLE = 'double',
    DATE = 'date',
    DATE_RANGE = 'dateRange',
    PHOTO = 'photo',
    ARRAY = 'array',
    LINKED_ID = 'linkedID',
    LINK = 'link',
    DOCUMENT_DOWNLOAD = 'documentDownload',
}

export enum STATUSES {
    SUCCESS = 'success',
    REGULAR = 'regular',
    WARNING = 'warning',
    ERROR = 'error',
}
